﻿using WorkmotorApp.iOS.Renderers;
using WorkmotorApp.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebViewRenderer))]

namespace WorkmotorApp.iOS.Renderers
{
    public class CustomWebViewRenderer : WebViewRenderer
    {
    }
}