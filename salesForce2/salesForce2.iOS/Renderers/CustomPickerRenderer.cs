﻿using UIKit;
using WorkmotorApp.iOS.Renderers;
using WorkmotorApp.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]

namespace WorkmotorApp.iOS.Renderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;
            Control.TextAlignment = UITextAlignment.Center;

            //Control.Font = UIFont.FromName("Helvetica bold", 14f);
            //Control.Font = UIFont.
            Control.Font = UIFont.BoldSystemFontOfSize(14);
            Control.Font = UIFont.SystemFontOfSize(16);
            //Control.Font = UIFont.SystemFontOfSize(14);
            //var view = e.NewElement as Picker;
            //this.Control.BorderStyle = UITextBorderStyle.None;
        }
    }
}