﻿using UIKit;
using WorkmotorApp.iOS.Renderers;
using WorkmotorApp.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NoUnderlineEditor), typeof(CustomEditorRenderer))]

namespace WorkmotorApp.iOS.Renderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        private string Placeholder { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control == null || !(Element is NoUnderlineEditor element)) return;
            Placeholder = element.Placeholder;
            Control.TextColor = UIColor.LightGray;
            Control.Text = Placeholder;

            Control.ShouldBeginEditing += textView =>
            {
                if (textView.Text != Placeholder) return true;
                textView.Text = "";
                textView.TextColor = UIColor.Black; // Text Color

                return true;
            };

            Control.ShouldEndEditing += textView =>
            {
                if (textView.Text != "") return true;
                textView.Text = Placeholder;
                textView.TextColor = UIColor.LightGray; // Placeholder Color

                return true;
            };
        }
    }
}