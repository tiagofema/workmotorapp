﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using WorkmotorApp.Models;
using Xamarin.Forms.Internals;

namespace WorkmotorApp
{
    [Preserve(AllMembers = true)]
    public class AppData
    {
        public AppData()
        {
            FuncionarioCollection = new ObservableCollection<FuncionarioModels>();
            ProdutoCollection = new ObservableCollection<ProdutoModels>();
            ClienteCollection = new ObservableCollection<ClienteModels>();
        }

        public List<EstadoModel> Estados { get; set; }
        public IList<FuncionarioModels> FuncionarioCollection { get; }
        public IList<ProdutoModels> ProdutoCollection { get; }

        public IList<ClienteModels> ClienteCollection { get; }

        public FuncionarioModels CurrentFuncionario { get; set; }
        public ProdutoModels CurrentProduto { get; set; }
        public OficinaModels CurrentOficina { get; set; }
        public string OficinaCnpj { get; set; }
        public ClienteModels CurrentCliente { get; set; }
        public ClienteVeiculoModels CurrentVeiculoCliente { get; set; }

        public OrdemServicoModels CurrentOrdemServico { get; set; }

        public List<ProdutoModels> CurrentProdutoList { get; set; }
        public List<ClienteModels> CurrentClienteList { get; set; }
        public List<CombustivelModels> Combustiveis { get; set; }
        //public List<OrdemServicoModels> CurrentOrdemServicoLista { get; set; }
        //public List<OrdemServicoModels> NovasOrdemServico { get; set; }
        public List<ClienteVeiculoModels> CurrentClienteVeiculo { get; set; }

        //variavel para os loadings
        public bool Logado { get; set; }
        public int ListaIndex { get; set; }
        public bool Busca { get; set; }
        //public bool BuscaCompleta { get; set; }
        public string BuscaString { get; set; }
        public string NumeroDocumento { get; set; } = "Nro. Documento: ";
        public int QuantidadeProdutos { get; set; } = 100;
        public int NumeroOrcamento { get; set; }
        //public bool Saida { get; set; }
        public bool AdicionarCliente { get; set; } = false;
        public bool Aplicacoes { get; set; } = false;

        public bool InsercaoClienteOs { get; set; } = false;

        //TESTANDO
        public bool AppCrash { get; set; } = true;

        public string Avarias { get; set; } = "";
        public string Defeitos { get; set; } = "";
        public string Observacao { get; set; } = "";

        //public string VideoYouTube { get; set; } = "https://m.youtube.com/channel/UCu1qIzZARaQU2HXyFKfowgg";
    }
}