﻿using System.Linq;
using System.Text.RegularExpressions;

namespace WorkmotorApp.Methods
{
    public class VerificadoresDocumentos
    {
        public static bool IsCnpj(string cnpj)
        {
            var multiplicador1 = new[] {5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
            var multiplicador2 = new[] {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
            int soma;
            int resto;
            cnpj = cnpj.Trim();
            //cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            var tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (var i = 0; i < 12; i++) soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            var digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (var i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto;
            return cnpj.EndsWith(digito);
        }

        public static bool IsCpf(string cpf)
        {
            var multiplicador1 = new[] {10, 9, 8, 7, 6, 5, 4, 3, 2};
            var multiplicador2 = new[] {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            var tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (var i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            var digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (var i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto;
            return cpf.EndsWith(digito);
        }

        public static string RemoverAcentos(string texto)
        {
            const string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
            const string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

            if (string.IsNullOrEmpty(texto)) return texto;

            for (var i = 0; i < comAcentos.Length; i++)
                texto = texto.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
            return texto;
        }

        public static bool IsDigitsOnly(string str)
        {
            return str.Cast<char>().All(c => c >= '0' && c <= '9');
        }

        public static string LimpezaNumeros(string input)
        {
            return string.IsNullOrEmpty(input) ? input : Regex.Replace(input, "[^0-9]", string.Empty);
        }

        public static string LimpezaGeral(string input)
        {
            return string.IsNullOrEmpty(input)
                ? input
                : Regex.Replace(input, "[^a-zA-Z0-9À-úçÇü\\s\\n.\\/,]", string.Empty);
            //return string.IsNullOrEmpty(input) ? input : Regex.Replace(input, "[^a-zA-Zà-üÀ-Ü0-9!@#$%¨&*()'\"; \\.,/ \\[ \\] \\{ \\} \\s]", string.Empty);
            //return string.IsNullOrEmpty(input) ? input : Regex.Replace(input, "[^a-zA-Zà-üÀ-Ü0-9\\s]", string.Empty);
        }

        public static string LimpezaPlaca(string input)
        {
            return string.IsNullOrEmpty(input) ? input : Regex.Replace(input, "[^a-zA-Z0-9]", string.Empty);
        }
    }
}