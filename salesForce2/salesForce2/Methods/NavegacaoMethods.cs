﻿using WorkmotorApp.Views;
using Xamarin.Forms;

namespace WorkmotorApp.Methods
{
    public static class NavegacaoMethods
    {
        public static void NavegaMenuPrincipal()
        {
            var paginaFuncionario = new NavigationPage(new MenuPrincipal2())
            {
                BarBackgroundColor = Color.FromHex("#223c74"),
                BarTextColor = Color.White
            };
            var rootPage = new MasterDetailBase
            {
                Master = new MenuLateral(),
                Detail = paginaFuncionario
            };
            Application.Current.MainPage = rootPage;
        }
    }
}