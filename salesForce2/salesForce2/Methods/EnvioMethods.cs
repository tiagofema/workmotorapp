﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Models;

namespace WorkmotorApp.Methods
{
    public class EnvioMethods
    {
        public static async Task<string> EnvioCliente(ClienteModels cliente)
        {
            var values = new Dictionary<string, string>
            {
                {"cliente", JsonConvert.SerializeObject(cliente)}
            };
            var content = new FormUrlEncodedContent(values);
            var response2 = await Client.Http.PostAsync("home/AdicionaCliente", content);
            return await response2.Content.ReadAsStringAsync();
        }

        public static async Task<string> EnvioCliente(ClienteModels cliente, ClienteVeiculoModels veiculo)
        {
            var values = new Dictionary<string, string>
            {
                {"cliente", JsonConvert.SerializeObject(cliente)},
                {"veiculo", JsonConvert.SerializeObject(veiculo)}
            };
            var content = new FormUrlEncodedContent(values);
            var response2 =
                await Client.Http.PostAsync("home/AdicionaClienteComVeiculo", content);
            return await response2.Content.ReadAsStringAsync();
        }

        public static async Task<string> EnvioVeiculo(ClienteVeiculoModels veiculo)
        {
            var values = new Dictionary<string, string>
            {
                {"veiculo", JsonConvert.SerializeObject(veiculo)}
            };
            var content = new FormUrlEncodedContent(values);
            var response2 =
                await Client.Http.PostAsync("home/AdicionarVeiculoCliente", content);
            return await response2.Content.ReadAsStringAsync();
        }

        public static async Task<string> RetornoMegaLaudo(string placa)
        {
            var response =
                await Client.MegaLaudo.GetAsync("v1/" + placa + "/aubbbYaKBzwgaYtkapWR" +DateTime.Today.Day);
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<List<CombustivelModels>> RetornaCombustiveis()
        {
            try
            {
                var response = await Client.Http.GetAsync("home/RetornarTodosCombustiveis");
                var resultado = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<CombustivelModels>>(resultado);
            }
            catch
            {
                return new List<CombustivelModels>();
            }
        }

        public static async Task<string> DeslogaFuncionario(int funcionarioId)
        {
            var values = new Dictionary<string, string>
            {
                {"funcionarioId", funcionarioId.ToString()}
            };

            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/DeslogaFuncionario", content);
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<List<ClienteVeiculoModels>> RetornarClienteVeiculos(int clienteId)
        {
            var values = new Dictionary<string, string>
            {
                {"documento", clienteId.ToString()}
            };
            var content = new FormUrlEncodedContent(values);
            var response =
                await Client.Http.PostAsync("home/RetornarClienteVeiculos", content);
            var resultado = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ClienteVeiculoModels>>(resultado);
        }

        public static async Task<string> RecebeOrdemServico(OrdemServicoModels ordemServico)
        {
            var values = new Dictionary<string, string>
            {
                {"ordem", JsonConvert.SerializeObject(ordemServico)}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/RecebeOrdemServico", content);
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<List<ProdutoModels>> RetornaProdutoIndex(int oficinaId, string buscaString,
            int quantidadeProdutos, int listaIndex, bool aplicacoes)
        {
            var values = new Dictionary<string, string>
            {
                {"id", oficinaId.ToString()},
                {"descricao", buscaString},
                {"quantidade", quantidadeProdutos.ToString()},
                {"index", listaIndex.ToString()},
                {"aplicacao", aplicacoes.ToString()}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/RetornarBuscaPecaIndex", content);
            var resultado = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ProdutoModels>>(resultado);
        }

        public static async Task<ClienteComVeiculoModels> RetornaCarroCliente(string placa, int oficinaId)
        {
            var values = new Dictionary<string, string>
            {
                {"placa", placa},
                {"oficinaId", oficinaId.ToString()}
            };

            var content = new FormUrlEncodedContent(values);
            var response =
                await Client.Http.PostAsync("home/RetornaCarroCliente", content);
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ClienteComVeiculoModels>(result);
        }

        public static async Task<string> AdicionarVeiculoCliente(string veiculo)
        {
            var values = new Dictionary<string, string>
            {
                {"veiculo", veiculo}
            };
            var content = new FormUrlEncodedContent(values);
            var response =
                await Client.Http.PostAsync("home/AdicionarVeiculoCliente", content);
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<List<CarroFipeModels>> RetornarTodosCarrosFipe(string veiculoText, int oficinaId)
        {
            var values = new Dictionary<string, string>
            {
                {"descricao", veiculoText},
                {"oficinaId", oficinaId.ToString()}
            };

            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/RetornarTodosCarrosFipe", content);
            var resultado = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<CarroFipeModels>>(resultado);
        }

        public static async Task<ClienteModels> RetornarClienteCpfCnpjPlaca(int oficinaId, string documento)
        {

            var response = await Client.Http.GetAsync("ClienteApi/PorCpfCnpjPlaca?id="+ oficinaId.ToString()+ "&cpfCnpj="+ documento);
            var resultado = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ClienteModels>(resultado);
        }
        public static async Task<ClienteModels> RetornarClienteCpfCnpj(int oficinaId, string documento)
        {
            using (var r = await Client.Http.GetAsync("home/RetornarClienteCpfCnpj?id=" + oficinaId.ToString() + "&cpfCnpj=" + documento))
            {
                var result = await r.Content.ReadAsStringAsync();
                var obj =  JsonConvert.DeserializeObject<ClienteModels>(result);
                return obj;
            }
        }

        public static async Task<List<ConsultaOsModels>> RetornarOrdensServicoConsulta(int oficinaId, string busca)
        {
            var values = new Dictionary<string, string>
            {
                {"id", oficinaId.ToString()},
                {"busca", busca}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/RetornarOrdemServico", content);
            var resultado = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ConsultaOsModels>>(resultado);
        }

        public static async Task<ClienteModels> RetornarCliente(int id)
        {
            var values = new Dictionary<string, string>
            {
                {"id", id.ToString()}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/RetornarClienteId", content);
            var resultado = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ClienteModels>(resultado);
        }
    }
}