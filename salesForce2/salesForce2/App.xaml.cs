﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using WorkmotorApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace WorkmotorApp
{
    [Preserve(AllMembers = true)]
    public partial class App : Application
    {
        public static NavigationPage Navigation = null;


        //public static WebView VideoView;
        //public static bool Videostart = false;
        //public static string Videourl;

        public App()
        {
            InitializeComponent();
            //if (Properties.ContainsKey("appData"))
            try
            {
                Task.Run(async () => { AppData = await DeserializaAppData((string) Properties["appData"]); }).Wait();
                //AppData = DeserializaAppData((string)Properties["appData"]);
                if (AppData.Logado == false) throw new Exception();
                //FAZER VaLidação de LOGIN
                if (!AppData.AppCrash)
                {
                    var a = "TRUE";
                    AppData.AppCrash = true;
                    Task.Run(async () => { a = await SerializeAppDataMethods.VerificaFuncionario(AppData); }).Wait();
                    switch (a)
                    {
                        case "FALSE":
                            throw new Exception();
                        case "ATIVO":
                            AppData.BuscaString = a;
                            throw new Exception();
                        default: break;
                    }
                }

                //
                var navigation = new NavigationPage(new MenuPrincipal2())
                    //var paginaFuncionario = new NavigationPage(new MenuPrincipal2())
                    {
                        BarBackgroundColor = Color.FromHex("#223c74"),
                        //BarBackgroundColor = Color.White,
                        BarTextColor = Color.White,
                        //BarTextColor = Color.FromHex("#223c74"),
                        Icon = "workmotologo2.png"
                    };
                //var paginaFuncionario = new MenuPrincipalFuncionario();
                var rootPage = new MasterDetailBase
                {
                    Master = new MenuLateral(),
                    //Detail = paginaFuncionario
                    Detail = navigation
                };
                MainPage = rootPage;
                //Properties["appData"] = SerializaAppData();
            }
            catch (Exception e)
            {
                AppData = new AppData();
                //Properties["appData"] = SerializaAppData();
                Inicializa();
            }

            //AppData = new AppData();
            //Inicializa();
        }

        public NavigationPage NavigationPage { private set; get; }
        public AppData AppData { set; get; }
        public static MasterDetailPage MasterDetail { get; set; }


        protected override void OnStart()
        {
            // Handle when your app starts
            //MESMA COISA DOS ANTERIORES, VERIFICAR SE O USUÁRIOS ESTAVA LOGADO ANTES DE FECHAR A APLICAÇÃO, RETORNANDO AO MENU PRINCIPAL AO INVÉS DA TELA DE LOGIN.
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Task.Run(async () => { Properties["appData"] = await SerializaAppData(); }).Wait();
            Task.Run(async () => { await SavePropertiesAsync(); }).Wait();
            //App.VideoView.Source = "about:blank";
            //FAZER ELE SALVAR O USUÁRIO CASO ESTEJA LOGADO. 
        }

        protected override void OnResume()
        {
            //if (Videostart)
            //    VideoView.Source = Videourl;

            Task.Run(async () => { AppData = await DeserializaAppData((string) Properties["appData"]); }).Wait();
            //AppData = DeserializaAppData((string)Properties["appData"]);
            if (!AppData.AppCrash)
                try
                {
                    if (AppData.Logado == false) throw new Exception();
                    var a = "TRUE";
                    Task.Run(async () => { a = await VerificaFuncionario(); }).Wait();
                    switch (a)
                    {
                        case "FALSE":
                            AppData.BuscaString = "";
                            throw new Exception();
                        case "ATIVO":
                            AppData.BuscaString = a;
                            throw new Exception();
                        default:
                            AppData.BuscaString = "INTERNET";
                            break;
                    }
                }
                catch
                {
                    //AppData = new AppData();
                    //AppData.BuscaString = "INTERNET";
                    Inicializa();
                }
            else
                try
                {
                    if (AppData.Logado == false) throw new Exception();
                    var a = "TRUE";
                    Task.Run(async () => { a = await VerificaFuncionario(); }).Wait();
                    switch (a)
                    {
                        case "FALSE":
                            AppData.BuscaString = "";
                            throw new Exception();
                        case "ATIVO":
                            AppData.BuscaString = a;
                            throw new Exception();
                        default:
                            AppData.BuscaString = "INTERNET";
                            break;
                    }

                    var navigation = new NavigationPage(new MenuPrincipal2())
                        //var paginaFuncionario = new NavigationPage(new MenuPrincipal2())
                        {
                            BarBackgroundColor = Color.FromHex("#223c74"),
                            //BarBackgroundColor = Color.White,
                            BarTextColor = Color.White,
                            //BarTextColor = Color.FromHex("#223c74"),
                            Icon = "workmotologo2.png"
                        };
                    //var paginaFuncionario = new MenuPrincipalFuncionario();
                    var rootPage = new MasterDetailBase
                    {
                        Master = new MenuLateral(),
                        //Detail = paginaFuncionario
                        Detail = navigation
                    };
                    MainPage = rootPage;
                }
                catch
                {
                    //AppData = new AppData();
                    //AppData.BuscaString = "INTERNET";
                    Inicializa();
                }

            // Handle when your app resumes
            //CASO O USUÁRIOS ESTIVESSE LOGADO, FAZER COM QUE ELE VOLTE AO MENU PRINCIPAL, E NÃO AO MENU INICIAL DE EMPRESA
        }

        public void Inicializa()
        {
            var paginaFuncionario = new NavigationPage(new TelaLoginEmpresa())
            {
                BarBackgroundColor = Color.FromHex("#223c74"),
                BarTextColor = Color.White,
                Icon = "workmotologo2.png"
            };
            var rootPage = new MasterDetailBase
            {
                Master = new MenuLateral(),
                Detail = paginaFuncionario
            };
            MainPage = rootPage;
        }

        public async Task<string> SerializaAppData()
        {
            AppData.CurrentCliente = new ClienteModels();
            AppData.CurrentClienteList = new List<ClienteModels>();
            AppData.CurrentProdutoList = new List<ProdutoModels>();
            AppData.CurrentProduto = new ProdutoModels();
            await DesativaFuncionario();
            await SavePropertiesAsync();
            return JsonConvert.SerializeObject(AppData);
        }

        public async Task<AppData> DeserializaAppData(string dados)
        {
            return JsonConvert.DeserializeObject<AppData>(dados);
        }

        public async Task DesativaFuncionario()
        {
            try
            {
                if (AppData.CurrentFuncionario != null)
                {
                    var values = new Dictionary<string, string>
                    {
                        {"funcionarioId", AppData.CurrentFuncionario.Id.ToString()}
                    };
                    //AppData.CurrentFuncionario = new FuncionarioModels();
                    var content = new FormUrlEncodedContent(values);
                    var response = await Client.Http.PostAsync("home/DesativaFuncionario", content);
                    await response.Content.ReadAsStringAsync();
                    AppData.AppCrash = false;
                }
            }
            catch
            {
                AppData.AppCrash = true;
            }
        }

        private async Task<string> VerificaFuncionario()
        {
            var values = new Dictionary<string, string>
            {
                {"funcionarioId", AppData.CurrentFuncionario.Id.ToString()}
            };

            var content = new FormUrlEncodedContent(values);
            var response = await Client.Http.PostAsync("home/RelogaFuncionario", content);
            AppData.BuscaString = "";
            return await response.Content.ReadAsStringAsync();
        }
    }
}