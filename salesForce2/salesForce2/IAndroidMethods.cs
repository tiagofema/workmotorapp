﻿using Xamarin.Forms.Internals;

namespace WorkmotorApp
{
    [Preserve(AllMembers = true)]
    public interface IAndroidMethods
    {
        void CloseApp();
    }
}