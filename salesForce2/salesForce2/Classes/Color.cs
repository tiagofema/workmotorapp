﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkmotorApp.Classes
{
    public static class Color
    {
            public static string ToString(this Xamarin.Forms.Color color)
            {
                var red = (int)(color.R * 255);
                var green = (int)(color.G * 255);
                var blue = (int)(color.B * 255);
                var alpha = (int)(color.A * 255);
                var hex = $"#{alpha:X2}{red:X2}{green:X2}{blue:X2}";
                return hex;
        }
    }
}
