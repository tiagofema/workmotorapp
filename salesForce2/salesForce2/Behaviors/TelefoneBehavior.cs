﻿using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace WorkmotorApp.Behaviors
{
    public class TelefoneBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;

            base.OnDetachingFrom(bindable);
        }

        private static void OnTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = (Entry) sender;

            entry.Text = FormataTelefone(entry.Text);
        }

        private static string FormataTelefone(string input)
        {
            var digitsRegex = new Regex(@"[^\d]");
            var digits = digitsRegex.Replace(input, "");

            if (digits.Length <= 7)
                return digits;

            if (digits.Length <= 10)
                return $"({digits.Substring(0, 2)}) {digits.Substring(2, 4)}-{digits.Substring(6)}";
            return
                $"({digits.Substring(0, 2)}) {digits.Substring(2, 1)} {digits.Substring(3, 4)}-{digits.Substring(7)}";
        }
    }
}