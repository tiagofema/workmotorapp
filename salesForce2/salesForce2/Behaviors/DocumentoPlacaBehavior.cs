﻿using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace WorkmotorApp.Behaviors
{
    public class DocumentoPlacaBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;

            base.OnDetachingFrom(bindable);
        }

        private static void OnTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = (Entry)sender;

            entry.Text = FormataDocumento(entry.Text);
        }

        private static string FormataDocumento(string input)
        {
            input = Regex.Replace(input, "[^a-zA-Zà-üÀ-Ü0-9\\s]", string.Empty);
            if (input.Length < 8) return input;
            var apenasDigitos = new Regex(@"[^\d]");
            var digits = apenasDigitos.Replace(input, "");
            if (digits.Length != input.Length) return input;
            if (digits.Length < 11) return input;
            if (digits.Length == 11)
                return
                    $"{digits.Substring(0, 3)}.{digits.Substring(3, 3)}.{digits.Substring(6, 3)}-{digits.Substring(9)}";
            return
                $"{digits.Substring(0, 2)}.{digits.Substring(2, 3)}.{digits.Substring(5, 3)}/{digits.Substring(8, 4)}-{digits.Substring(12)}";
        }
    }
}