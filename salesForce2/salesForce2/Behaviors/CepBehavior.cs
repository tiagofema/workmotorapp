﻿using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace WorkmotorApp.Behaviors
{
    public class CepBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;

            base.OnDetachingFrom(bindable);
        }

        private static void OnTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = (Entry) sender;

            entry.Text = FormataCep(entry.Text);
        }

        private static string FormataCep(string input)
        {
            var digitsRegex = new Regex(@"[^\d]");
            var digits = digitsRegex.Replace(input, "");

            return digits.Length <= 5 ? digits : $"{digits.Substring(0, 5)}-{digits.Substring(5)}";
        }
    }
}