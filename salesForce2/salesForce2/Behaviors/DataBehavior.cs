﻿using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace WorkmotorApp.Behaviors
{
    internal class DataBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;

            base.OnDetachingFrom(bindable);
        }

        private static void OnTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = (Entry) sender;

            entry.Text = FormataData(entry.Text);
        }

        private static string FormataData(string input)
        {
            var digitsRegex = new Regex(@"[^\d]");
            var digits = digitsRegex.Replace(input, "");


            if (digits.Length <= 3) return digits;
            if (digits.Length <= 4) return $"{digits.Substring(0, 2)}/{digits.Substring(2, 2)}";
            if (digits.Length <= 6) return $"{digits.Substring(0, 2)}/{digits.Substring(2, 2)}/{digits.Substring(4)}";
            return $"{digits.Substring(0, 2)}/{digits.Substring(2, 2)}/{digits.Substring(4)}";
        }
    }
}