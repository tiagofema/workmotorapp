﻿using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class ClienteVeiculoModels
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int CarroFipeId { get; set; }
        public int CombustivelId { get; set; }
        public string Placa { get; set; }
        public string Ano { get; set; }
        public string Cor { get; set; }
        public decimal Quilometragem { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string CombustivelTipo { get; set; }
    }
}