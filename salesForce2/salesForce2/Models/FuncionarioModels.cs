﻿using System;
using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class FuncionarioModels
    {
        [Preserve(AllMembers = true)] public int Id { get; set; }

        public int OficinaId { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public DateTime DataModificacao { get; set; }
        public string Cargo { get; set; }
        public string Codigo { get; set; }
        public bool Logado { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataDemissao { get; set; }
    }
}