﻿using System;
using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class OrdemServicoModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public int FuncionarioId { get; set; }
        public int CarroFipeId { get; set; }
        public int CombustivelId { get; set; }
        public int ClienteId { get; set; }
        public int NumeroOrcamento { get; set; }
        public int Ano { get; set; }
        public decimal Quilometragem { get; set; }
        public string Observacao { get; set; }
        public string Defeitos { get; set; }
        public string Avarias { get; set; }
        public string Placa { get; set; }
        public int WorkMotorId { get; set; }
        public DateTime DataEmissao { get; set; }
        public string Cor { get; set; }

        public string Status { get; set; }
        public decimal TotalPecas { get; set; }
        public decimal TotalServicos { get; set; }
        public decimal Total { get; set; }
        public decimal DescProdutos { get; set; }
        public decimal DescServicos { get; set; }
    }
}