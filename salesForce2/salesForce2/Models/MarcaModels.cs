﻿using System;
using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class MarcaModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public string Codigo { get; set; }
        public string Tipo { get; set; }
        public string Descricao { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}