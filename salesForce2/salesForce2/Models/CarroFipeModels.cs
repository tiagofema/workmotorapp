﻿using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class CarroFipeModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public string CodigoFipe { get; set; }
        public string Descricao { get; set; }
        public string AnoInicial { get; set; }
        public string AnoFinal { get; set; }
    }
}