﻿using System;
using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class OficinaModels
    {
        public int Id { get; set; }
        public string NomeFantasia { get; set; }
        public string RazaoSocial { get; set; }
        public string Cnpj { get; set; }
        public string Codigo { get; set; }
        public int MunicipioId { get; set; }
        public bool Ativa { get; set; }
        public DateTime DataUltimaModificacao { get; set; }
        public string Senha { get; set; }
        public int Acesso { get; set; }
        public bool EnviandoOrdem { get; set; }
        public bool EnviandoCliente { get; set; }
        public bool EnviandoVeiculo { get; set; }
    }
}