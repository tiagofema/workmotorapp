﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class ClienteModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public int MunicipioIbge { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CnpjCpf { get; set; }
        public string InscricaoEstadual { get; set; }
        public string Endereco { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Cep { get; set; }
        public int Ddd { get; set; }
        public string Telefone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        //public string Documento { get; set; }

        public int Codigo { get; set; }
        public int CelularDdd { get; set; }
        public string Celular { get; set; }
        public DateTime DataNascimento { get; set; }
        public char Sexo { get; set; }

        private char Inicial
        {
            get
            {
                var val = ' ';
                if (!string.IsNullOrEmpty(RazaoSocial)) val = Convert.ToChar(RazaoSocial.Replace(" ", "")?.Substring(0, 1));
                if (!string.IsNullOrEmpty(NomeFantasia)) val = Convert.ToChar(NomeFantasia.Replace(" ", "")?.Substring(0, 1));
                return val;
            }
        }

        public string SiglaNome
        {
            get
            {
                var val = "";
                if (!string.IsNullOrEmpty(RazaoSocial)) val =RazaoSocial.Replace(" ","")?.Substring(0, 2);
                if (!string.IsNullOrEmpty(NomeFantasia)) val = NomeFantasia.Replace(" ", "")?.Substring(0, 2);
                return val;

            }
        }

        public string CorInicial
        {
            get
            {
                switch (Convert.ToChar(Convert.ToString(Inicial)?.ToLower()))
                {
                    case 'a': return Classes.Color.ToString(Color.Blue);
                    case 'b': return Classes.Color.ToString(Color.BlueViolet);
                    case 'c': return Classes.Color.ToString(Color.Brown);
                    case 'd': return Classes.Color.ToString(Color.CadetBlue);
                    case 'e': return Classes.Color.ToString(Color.Coral);
                    case 'f': return Classes.Color.ToString(Color.CornflowerBlue);
                    case 'g': return Classes.Color.ToString(Color.YellowGreen);
                    case 'h': return Classes.Color.ToString(Color.Yellow);
                    case 'i': return Classes.Color.ToString(Color.Violet);
                    case 'j': return Classes.Color.ToString(Color.Tomato);
                    case 'k': return Classes.Color.ToString(Color.Teal);
                    case 'l': return Classes.Color.ToString(Color.Sienna);
                    case 'm': return Classes.Color.ToString(Color.SteelBlue);
                    case 'n': return Classes.Color.ToString(Color.SeaGreen);
                    case 'o': return Classes.Color.ToString(Color.Red);
                    case 'p': return Classes.Color.ToString(Color.Orange);
                    case 'q': return Classes.Color.ToString(Color.Olive);
                    case 'r': return Classes.Color.ToString(Color.SaddleBrown);
                    case 's': return Classes.Color.ToString(Color.Navy);
                    case 't': return Classes.Color.ToString(Color.Peru);
                    case 'u': return Classes.Color.ToString(Color.DarkRed);
                    case 'v': return Classes.Color.ToString(Color.DarkGreen);
                    case 'w': return Classes.Color.ToString(Color.DarkBlue);
                    case 'x': return Classes.Color.ToString(Color.DarkSlateGray);
                    case 'y': return Classes.Color.ToString(Color.DarkMagenta);
                    case 'z': return Classes.Color.ToString(Color.SaddleBrown);
                    default: return Classes.Color.ToString(Color.Silver);
                }
            }
        }
    }
}