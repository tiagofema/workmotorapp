﻿using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class ClienteComVeiculoModels
    {
        public ClienteModels Cliente { get; set; }
        public ClienteVeiculoModels Veiculo { get; set; }
    }
}