﻿using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class CombustivelModels
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}