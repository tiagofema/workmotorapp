﻿

using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace WorkmotorApp.Models
{
    [Preserve(AllMembers = true)]
    public class ProdutoModels
    {
        public int Id { get; set; }
        public int OficinaId { get; set; }
        public int MarcaId { get; set; }
        public int FamiliaId { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string ReferenciaFabricante { get; set; }
        public string Modelo { get; set; }
        public string Tamanho { get; set; }
        public decimal Peso { get; set; }
        public string Local { get; set; }
        public decimal ValorVenda { get; set; }
        public string Unidade { get; set; }
        public DateTime DataModificacao { get; set; }
        public double Quantidade { get; set; }
        public string Aplicacao { get; set; }
        public string CodigoReferencia { get; set; }

        public string FamiliaDescricao { get; set; }
        public string MarcaDescricao { get; set; }

        private char Inicial
        {
            get
            {
                var val = ' ';
                if (!string.IsNullOrEmpty(Descricao)) val = Convert.ToChar(Descricao.Replace(" ", "")?.Substring(0, 1));
                return val;
            }
        }

        public string SiglaNome
        {
            get
            {
                var val = "";
                if (!string.IsNullOrEmpty(Descricao))val = Descricao.Replace(" ", "")?.Substring(0, 2);
                return val;

            }
        }

        public string CorInicial
        {
            get
            {
                switch (Convert.ToChar(Convert.ToString(Inicial)?.ToLower()))
                {
                    case 'a': return Classes.Color.ToString(Color.Blue);
                    case 'b': return Classes.Color.ToString(Color.BlueViolet);
                    case 'c': return Classes.Color.ToString(Color.Brown);
                    case 'd': return Classes.Color.ToString(Color.CadetBlue);
                    case 'e': return Classes.Color.ToString(Color.Coral);
                    case 'f': return Classes.Color.ToString(Color.CornflowerBlue);
                    case 'g': return Classes.Color.ToString(Color.YellowGreen);
                    case 'h': return Classes.Color.ToString(Color.Yellow);
                    case 'i': return Classes.Color.ToString(Color.Violet);
                    case 'j': return Classes.Color.ToString(Color.Tomato);
                    case 'k': return Classes.Color.ToString(Color.Teal);
                    case 'l': return Classes.Color.ToString(Color.Sienna);
                    case 'm': return Classes.Color.ToString(Color.SteelBlue);
                    case 'n': return Classes.Color.ToString(Color.SeaGreen);
                    case 'o': return Classes.Color.ToString(Color.Red);
                    case 'p': return Classes.Color.ToString(Color.Orange);
                    case 'q': return Classes.Color.ToString(Color.Olive);
                    case 'r': return Classes.Color.ToString(Color.SaddleBrown);
                    case 's': return Classes.Color.ToString(Color.Navy);
                    case 't': return Classes.Color.ToString(Color.Peru);
                    case 'u': return Classes.Color.ToString(Color.DarkRed);
                    case 'v': return Classes.Color.ToString(Color.DarkGreen);
                    case 'w': return Classes.Color.ToString(Color.DarkBlue);
                    case 'x': return Classes.Color.ToString(Color.DarkSlateGray);
                    case 'y': return Classes.Color.ToString(Color.DarkMagenta);
                    case 'z': return Classes.Color.ToString(Color.SaddleBrown);
                    default: return Classes.Color.ToString(Color.Silver);
                }
            }
        }
    }
}