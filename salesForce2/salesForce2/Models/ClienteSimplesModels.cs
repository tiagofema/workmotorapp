﻿namespace WorkmotorApp.Models
{
    public class ClienteSimplesModels
    {
        public string Placa { get; set; }
        public string Ano { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Cor { get; set; }
        public string Nome { get; set; }
        public string CpfCnpj { get; set; }
    }
}