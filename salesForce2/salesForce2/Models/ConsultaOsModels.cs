﻿using System;
using Xamarin.Forms;

namespace WorkmotorApp.Models
{
    public class ConsultaOsModels
    {
        public DateTime Data { get; set; }
        public string Placa { get; set; }
        public string VeiculoDescricao { get; set; }
        public string Nome { get; set; }
        public string Status { get; set; }
        public double TotalPecas { get; set; }
        public double TotalServicos { get; set; }
        public double Total { get; set; }
        public int ClienteId { get; set; }
        public decimal DescontoProdutos { get; set; }
        public decimal DescontoServicos { get; set; }

        public double TotalServicosBruto => TotalServicos + (double)DescontoServicos; 
        public double TotalPecasBruto => TotalPecas + (double)DescontoProdutos;




        private char Inicial
        {
            get
            {
                var val = ' ';
                if (!string.IsNullOrEmpty(Nome)) val = Convert.ToChar(Nome.Replace(" ", "")?.Substring(0, 1));
                return val;
            }
        }

        public string SiglaNome
        {
            get
            {
                var val = "";
                if (!string.IsNullOrEmpty(Nome)) val = Nome.Replace(" ", "")?.Substring(0, 2);
                return val;

            }
        }

        public string CorInicial
        {
            get
            {
                switch (Convert.ToChar(Convert.ToString(Inicial)?.ToLower()))
                {
                    case 'a': return Classes.Color.ToString(Color.Blue);
                    case 'b': return Classes.Color.ToString(Color.BlueViolet);
                    case 'c': return Classes.Color.ToString(Color.Brown);
                    case 'd': return Classes.Color.ToString(Color.CadetBlue);
                    case 'e': return Classes.Color.ToString(Color.Coral);
                    case 'f': return Classes.Color.ToString(Color.CornflowerBlue);
                    case 'g': return Classes.Color.ToString(Color.YellowGreen);
                    case 'h': return Classes.Color.ToString(Color.Yellow);
                    case 'i': return Classes.Color.ToString(Color.Violet);
                    case 'j': return Classes.Color.ToString(Color.Tomato);
                    case 'k': return Classes.Color.ToString(Color.Teal);
                    case 'l': return Classes.Color.ToString(Color.Sienna);
                    case 'm': return Classes.Color.ToString(Color.SteelBlue);
                    case 'n': return Classes.Color.ToString(Color.SeaGreen);
                    case 'o': return Classes.Color.ToString(Color.Red);
                    case 'p': return Classes.Color.ToString(Color.Orange);
                    case 'q': return Classes.Color.ToString(Color.Olive);
                    case 'r': return Classes.Color.ToString(Color.SaddleBrown);
                    case 's': return Classes.Color.ToString(Color.Navy);
                    case 't': return Classes.Color.ToString(Color.Peru);
                    case 'u': return Classes.Color.ToString(Color.DarkRed);
                    case 'v': return Classes.Color.ToString(Color.DarkGreen);
                    case 'w': return Classes.Color.ToString(Color.DarkBlue);
                    case 'x': return Classes.Color.ToString(Color.DarkSlateGray);
                    case 'y': return Classes.Color.ToString(Color.DarkMagenta);
                    case 'z': return Classes.Color.ToString(Color.SaddleBrown);
                    default: return Classes.Color.ToString(Color.Silver);
                }
            }
        }
    }
}