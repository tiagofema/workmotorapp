﻿using System;

namespace WorkmotorApp.ViewModels
{
    public class OficinaViewModel : ViewModelBase
    {
        private bool _ativa;
        private DateTime _dataUltimaModificacao;
        private int _id, _municipioId;
        private string _nomeFantasia, _razaoSocial, _cnpj, _codigo, _senha;

        public int Id
        {
            set => SetProperty(ref _id, value);
            get => _id;
        }

        public int MunicipioId
        {
            set => SetProperty(ref _municipioId, value);
            get => _municipioId;
        }

        public string NomeFantasia
        {
            set => SetProperty(ref _nomeFantasia, value);
            get => _nomeFantasia;
        }

        public string Senha
        {
            set => SetProperty(ref _senha, value);
            get => _senha;
        }

        public string RazaoSocial
        {
            set => SetProperty(ref _razaoSocial, value);
            get => _razaoSocial;
        }

        public string Cnpj
        {
            set => SetProperty(ref _cnpj, value);
            get => _cnpj;
        }

        public string Codigo
        {
            set => SetProperty(ref _codigo, value);
            get => _codigo;
        }

        public DateTime DataUltimaModificacao
        {
            set => SetProperty(ref _dataUltimaModificacao, value);
            get => _dataUltimaModificacao;
        }

        public bool Ativa
        {
            set => SetProperty(ref _ativa, value);
            get => _ativa;
        }
    }
}