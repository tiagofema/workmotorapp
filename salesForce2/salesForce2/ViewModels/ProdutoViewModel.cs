﻿using System;

namespace WorkmotorApp.ViewModels
{
    public class ProdutoViewModel : ViewModelBase

    {
        private string _codigo,
            _descricao,
            _referenciaFabricante,
            _modelo,
            _tamanho,
            _local,
            _unidade,
            _familiaDescricao,
            _marcaDescricao;

        private DateTime _dataModificacao;
        private int _id, _oficinaId, _marcaId, _familiaId;
        private decimal _peso, _valorVenda;

        public int Id
        {
            set => SetProperty(ref _id, value);
            get => _id;
        }

        public int OficinaId
        {
            set => SetProperty(ref _oficinaId, value);
            get => _oficinaId;
        }

        public int MarcaId
        {
            set => SetProperty(ref _marcaId, value);
            get => _marcaId;
        }

        public int FamiliaId
        {
            set => SetProperty(ref _familiaId, value);
            get => _familiaId;
        }

        public string Codigo
        {
            set => SetProperty(ref _codigo, value);
            get => _codigo;
        }

        public string Descricao
        {
            set => SetProperty(ref _descricao, value);
            get => _descricao;
        }

        public string ReferenciaFabricante
        {
            set => SetProperty(ref _referenciaFabricante, value);
            get => _referenciaFabricante;
        }

        public string Modelo
        {
            set => SetProperty(ref _modelo, value);
            get => _modelo;
        }

        public string Tamanho
        {
            set => SetProperty(ref _tamanho, value);
            get => _tamanho;
        }

        public decimal Peso
        {
            set => SetProperty(ref _peso, value);
            get => _peso;
        }

        public string Local
        {
            set => SetProperty(ref _local, value);
            get => _local;
        }

        public decimal ValorVenda
        {
            set => SetProperty(ref _valorVenda, value);
            get => _valorVenda;
        }

        public string Unidade
        {
            set => SetProperty(ref _unidade, value);
            get => _unidade;
        }

        public DateTime DataModificacao
        {
            set => SetProperty(ref _dataModificacao, value);
            get => _dataModificacao;
        }

        public string FamiliaDescricao
        {
            set => SetProperty(ref _familiaDescricao, value);
            get => _familiaDescricao;
        }

        public string MarcaDescricao
        {
            set => SetProperty(ref _marcaDescricao, value);
            get => _marcaDescricao;
        }
    }
}