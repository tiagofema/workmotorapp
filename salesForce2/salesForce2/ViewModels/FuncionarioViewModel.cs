﻿using System;

namespace WorkmotorApp.ViewModels
{
    public class FuncionarioViewModel : ViewModelBase
    {
        private DateTime _dataModificacao;
        private int _id, _oficinaId;
        private string _nome, _senha;


        public int Id
        {
            set => SetProperty(ref _id, value);
            get => _id;
        }

        public int OficinaId
        {
            set => SetProperty(ref _oficinaId, value);
            get => _oficinaId;
        }

        public string Nome
        {
            set => SetProperty(ref _nome, value);
            get => _nome;
        }

        public string Senha
        {
            set => SetProperty(ref _senha, value);
            get => _senha;
        }

        public DateTime DataModificacao
        {
            set => SetProperty(ref _dataModificacao, value);
            get => _dataModificacao;
        }
    }
}