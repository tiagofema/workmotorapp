﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace WorkmotorApp.Renderers
{
    [Preserve(AllMembers = true)]
    public class NoUnderlineEditor : Editor
    {
        //public static readonly BindableProperty PlaceholderProperty =
        //    BindableProperty.Create<NoUnderlineEditor, string>(view => view.Placeholder, String.Empty);

        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(NoUnderlineEditor), string.Empty);

        public string Placeholder
        {
            get => (string) GetValue(PlaceholderProperty);

            set => SetValue(PlaceholderProperty, value);
        }
    }
}