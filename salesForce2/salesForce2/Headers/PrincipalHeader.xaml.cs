﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Headers
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrincipalHeader : ContentPage
    {
        public PrincipalHeader()
        {
            InitializeComponent();
        }
    }
}