﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetalharOs : ContentPage
	{
		public DetalharOs(ConsultaOsModels os)
		{
			InitializeComponent();
		    this.BindingContext = os;
		}
	}
}