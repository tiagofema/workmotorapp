﻿using System;
using Newtonsoft.Json;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuLateral : ContentPage
    {
        public MenuLateral()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void DeslogarBotao(object sender, EventArgs e)
        {
            Deslogar();
        }

        private async void Deslogar()
        {
            var resposta = await DisplayAlert("Logout", "Deseja realmente sair?", "Sim", "Não");
            if (!resposta) return;
            var appData = (AppData) BindingContext;
            var resultado = await EnvioMethods.DeslogaFuncionario(appData.CurrentFuncionario.Id);
            switch (resultado)
            {
                case "TRUE": break;
                case "FALSE":
                    await DisplayAlert("Erro",
                        "Não foi possivel conectar-se ao servidor para concluir a operação de logoff. Verifique sua conexão com a internet e tente novamente.",
                        "Ok");
                    return;
                default:
                    await DisplayAlert("Erro",
                        "Não foi possivel conectar-se ao servidor para concluir a operação de logoff. Verifique sua conexão com a internet e tente novamente.",
                        "Ok");
                    return;
            }

            appData.CurrentOficina = new OficinaModels();
            appData.CurrentFuncionario = new FuncionarioModels();
            Application.Current.Properties["appData"] = JsonConvert.SerializeObject(appData);
            Application.Current.MainPage = new NavigationPage(new TelaLoginEmpresa());
        }

        private async void BotaoConsultaPecas_OnClicked(object sender, EventArgs e)
        {
            try
            {
                App.MasterDetail.IsPresented = false;
                App.MasterDetail.Detail.Title = "Menu";
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaTodasPecas());
            }
            catch
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }


        private async void BotaoConsultaDeClientes_OnClicked(object sender, EventArgs e)
        {
            try
            {
                App.MasterDetail.IsPresented = false;
                App.MasterDetail.Detail.Title = "Menu";
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaTodosClientes());
            }
            catch
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }

        private async void BotaoAberturaDeOs_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            try
            {
                appData.CurrentOrdemServico = new OrdemServicoModels();
                App.MasterDetail.IsPresented = false;
                App.MasterDetail.Detail.Title = "Menu";
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaOrdemServico());
            }
            catch
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }

        private async void BotaoCadastrarCliente_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            try
            {
                appData.AdicionarCliente = true;
                App.MasterDetail.IsPresented = false;
                App.MasterDetail.Detail.Title = "Menu";
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaAdicionarCliente());
            }
            catch
            {
                appData.AdicionarCliente = false;
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }

        private async void BotaoConsultaDeOs_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            try
            {
                appData.CurrentOrdemServico = new OrdemServicoModels();
                App.MasterDetail.IsPresented = false;
                App.MasterDetail.Detail.Title = "Menu";
                await App.MasterDetail.Detail.Navigation.PushAsync(new PaginaConsultaOrdemServico());
            }
            catch
            {
                await DisplayAlert("Erro", "Houve algum erro na navegação. Tente novamente.", "Ok");
            }
        }
    }
}