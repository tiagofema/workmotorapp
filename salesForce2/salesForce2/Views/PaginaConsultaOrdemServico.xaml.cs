﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaConsultaOrdemServico
    {
        public PaginaConsultaOrdemServico()
        {
            InitializeComponent();
            NumeroDocumento.Text = "";
        }
        private async void BuscaDocumento_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            ActivitySwitch(true);
            if (string.IsNullOrEmpty(NumeroDocumento.Text))
            {
                await DisplayAlert("", "Por favor digite um documento a ser procurado.", "Ok");
                ActivitySwitch(false);
                return;
            }
            try
            {
                var documento = NumeroDocumento.Text.Replace("-", "");
                if (NumeroDocumento.Text.Length > 7) documento = VerificadoresDocumentos.LimpezaNumeros(NumeroDocumento.Text);
                var listaOrdensServico = await EnvioMethods.RetornarOrdensServicoConsulta(appData.CurrentOficina.Id, documento);
                listaOrdensServico = listaOrdensServico.OrderByDescending(o => o?.Data).ToList();
                OrdemServicoListView.ItemsSource = listaOrdensServico;
                OrdemServicoListView.IsVisible = listaOrdensServico.Count > 0;
                if (listaOrdensServico.Count == 0)
                {
                    await DisplayAlert("", "Não foi encontrada a OS com os dados informados.",
                        "Ok");
                    OrdemServicoListView.ItemsSource = new List<ConsultaOsModels>();
                }
            }
            catch
            {
                await DisplayAlert("", "Não foi encontrada a OS com os dados informados.",
                    "Ok");
                OrdemServicoListView.ItemsSource = new List<ConsultaOsModels>();
            }

            ActivitySwitch(false);
        }

        private void NumeroDocumento_OnCompleted(object sender, EventArgs e)
        {
            BuscaDocumento_OnClicked(new object(), new EventArgs());
        }

        private async void OrdemServicoListView_OnItemSelected(object sender, SelectedItemChangedEventArgs item)
        {
            if (item.SelectedItem == null) return;
            var os = (ConsultaOsModels)item.SelectedItem;
            OrdemServicoListView.SelectedItem = null;
            await Navigation.PushAsync(new DetalharOs(os));
        }

        private void OrdemServicoListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            //
        }

        private void ActivitySwitch(bool valor)
        {
            Activity.IsRunning = valor;
            LoadingStack.IsVisible = valor;
        }

        private void NumeroDocumento_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var input = NumeroDocumento?.Text;
            if (input?.Length == 0)
            {
                NumeroDocumento.Keyboard = Keyboard.Default;
                return;
            }
            input = Regex.Replace(input ?? "", "[^a-zA-Zà-üÀ-Ü0-9\\s]", string.Empty);
            var apenasDigitos = new Regex(@"[^\d]");
            var digits = apenasDigitos.Replace(input.Substring(0, 1) ?? "", "");
            if (digits.Length > 0)
            {
                NumeroDocumento.Keyboard = Keyboard.Numeric;
            }
            else
            {
                var contagem = input.ToUpper()?.Cast<char>().Count(char.IsUpper);
                NumeroDocumento.Keyboard = contagem == 3 ? Keyboard.Numeric : Keyboard.Default;
            }
        }
    }
}