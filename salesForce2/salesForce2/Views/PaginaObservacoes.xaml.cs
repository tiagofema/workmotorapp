﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaObservacoes : ContentPage
    {
        //public AppData AppData { set; get; }


        public PaginaObservacoes()
        {
            InitializeComponent();
            PegarTextos();
        }

        private void PegarTextos()
        {
            var appData = (AppData) BindingContext;

            Avarias.Text = appData.Avarias;
            Defeitos.Text = appData.Defeitos;
            Observacao.Text = appData.Observacao;
        }

        private async void GravarObservacoes_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            try
            {
                appData.Avarias = Avarias.Text;
                appData.Defeitos = Defeitos.Text;
                appData.Observacao = Observacao.Text;
                await Navigation.PopAsync();
            }
            catch
            {
                var resposta = await DisplayAlert("Erro", "As observações não foram salvas. Deseja tentar novamente?",
                    "Sim", "Não");
                if (!resposta) return;
                await Navigation.PopAsync();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage) Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
            }
        }

        //protected override bool OnBackButtonPressed()
        //{

        //    var appData = (AppData) BindingContext;
        //    try
        //    {
        //        if (Avarias.Text != appData.Avarias || Defeitos.Text != appData.Defeitos ||
        //            Observacao.Text != appData.Observacao)
        //        {
        //            var resposta = DisplayAlert("Alerta",
        //                "Deseja realmente sair sem salvar? Todos os dados serão perdido.", "Sim", "Não");
        //            return resposta.Result;
        //        }
        //        Navigation.PopModalAsync();
        //    }
        //    catch (Exception e)
        //    {
        //        DisplayAlert("Alerta", e.Message, "Ok");
        //    }
        //    return true;

        //}
    }
}