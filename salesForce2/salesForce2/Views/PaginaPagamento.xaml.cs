﻿using System;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaPagamento : ContentPage
    {
        //public const string ClientId = "433344286963256";
        //public const string ClientSecret = "A6Mh8lO1NfnkNuzoG0wspaIgI4floS9a";
        //public const string PublicKey = "TEST-91ea58de-84f8-4f9c-a39c-8c1fa6dd6b6b";
        //public const string AcessToken = "TEST-433344286963256-052113-71d48e4a4da788aeb93d260a256aa930-24577770";

        public const string PointPackage = "com.mercadopago.merchant";

        public const string ResultStatusOk = "OK";
        public const string ResultStatusFailed = "FAILED";
        public const string CreditCard = "credit_card";
        public const string DebitCard = "debit_card";
        public const string Action = "com.mercadopago.PAYMENT_ACTION";
        public const string Link = "https://secure.mlstatic.com/org-img/point/app/index.html";

        public const string ResultPaymentId = "paymentId";
        //amount: The amount that will be charged to the customer(*).
        //description: A description of the transaction(Max.: 20 characters) (*).
        //external_reference: The reference code of your system, the same one that will allow you to conciliate your purchase order with the payment.
        //notification_url: The URL where you will receive the notification of that payment.
        //payer_email: The payer’s email.
        //success_url: The URL to which the user will be redirected after an approved payment.
        //fail_url: The URL to which the user will be redirected after a declined payment.

        //RESULTS
        //amount  -  The amount charged
        //result_status  -  The payment's status	Values "OK" or "FAILED"
        //payment_id  -  The payment's id	only in sucessfull payments
        //card_type  -  the card type   Values "credit_card" or "debit_card"
        //installments  -  the installments
        //error  -  error reason    Only in "FAILED" transactions
        //error_detail  -  error details   Only in "FAILED" transactions

        public const string FailUrl = "192.168.0.115:62838/home/";
        public const string SucessUrl = "192.168.0.115:62838/home";

        public PaginaPagamento()
        {
            InitializeComponent();
        }

        public static HttpClient Endereco => new HttpClient
        {
            BaseAddress = new Uri("https://www.mercadopago.com/point/integrations?")
        };

        private void BotaoPagar_OnClicked(object sender, EventArgs e)
        {
            //Xamarin.Forms.Device.OpenUri(new Uri("whatsapp://send?text="+TextoTeste.Text));
            var batata = new MercadoPago
            {
                amount = 20,
                description = "Teste"
            };
            Device.OpenUri(new Uri(
                "https://www.mercadopago.com/point/integrations?amount=1&description=Teste&card_type=debit_card&success_url=http://workmotor.com.br/sucesso&fail_url=workmotor.com.br/fracasso"));
            //Xamarin.Forms.Device.OpenUri(new Uri("https://secure.mlstatic.com/org-img/point/app/index.html" + JsonConvert.SerializeObject(batata)));
        }

        public class MercadoPago
        {
            public float amount = 20;
            public string description = "Teste";
            public int external_reference = 20;
            public string fail_url = "";
            public string notification_url = "";
            public string payer_email = "negri701@gmail.com";
            public string success_url = "";
        }
    }
}