﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaTodasPecas : ContentPage
    {
        public PaginaTodasPecas()
        {
            InitializeComponent();
            TransformaPagina();
        }

        public ObservableCollection<ProdutoModels> Produtos { get; set; }
        private List<ProdutoModels> ListaProdutos { get; set; }
        public bool BuscaCompleta { get; set; }

        private void TransformaPagina()
        {
            var appData = (AppData) BindingContext;
            if (!appData.Aplicacoes) return;
            Busca.Placeholder = "VEÍCULO";
            Title = "PEÇAS APLICAÇÃO";
            BotaoCodigoBarras.IsVisible = false;
        }


        private void ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var items = Produtos;
            if (BuscaCompleta) return;
            if (PecasListView.IsRefreshing) return;
            if (items.Count < 10)
            {
                BuscaCompleta = true;
                return;
            }

            try
            {
                if (items != null && e.Item == items[items.Count - 30]) CarregarMaisProdutos();
            }
            catch
            {
                //ignore
            }

            PecasListView.ItemsSource = Produtos;
        }

        private void ListarProdutos(List<ProdutoModels> listaProdutos)
        {
            Produtos = new ObservableCollection<ProdutoModels>();
            foreach (var f in listaProdutos) Produtos.Add(f);
            if (Produtos.Count < 1)
                DisplayAlert("Erro", "Não foi encontrado nenhum produto. Verifique a pesquisa e tente novamente.",
                    "Ok");
            PecasListView.ItemsSource = Produtos;
            PecasListView.IsVisible = Produtos.Count > 0;
        }

        private void ProdutoSelecionadoListView(object sender, SelectedItemChangedEventArgs item)
        {
            var appData = (AppData) BindingContext;
            appData.CurrentProduto = (ProdutoModels) item.SelectedItem;
            if (item.SelectedItem == null) return;
            PecasListView.SelectedItem = null;

            IrPaginaPecas();
        }

        private async void IrPaginaPecas()
        {
            await Navigation.PushAsync(new PaginaPeca());
        }

        private async void BuscaPecas(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            BuscaCompleta = false;
            appData.Busca = true;
            appData.ListaIndex = 0;
            try
            {
                appData.BuscaString = Regex.Replace(VerificadoresDocumentos.RemoverAcentos(Busca.Text),
                    "[^A-Za-z0-9 \\. -]", string.Empty);
            }
            catch
            {
                appData.BuscaString = "";
            }

            PecasListView.IsRefreshing = true;
            ListaProdutos = await BuscaFuncao(true);
            PecasListView.IsRefreshing = false;
            ListarProdutos(ListaProdutos);
        }

        private async void CarregarMaisProdutos()
        {
            var appData = (AppData) BindingContext;
            PecasListView.IsRefreshing = true;
            appData.ListaIndex += appData.QuantidadeProdutos;
            try
            {
                await BuscaFuncao(appData.Busca);
            }
            catch
            {
                await DisplayAlert("Alerta",
                    "Problema na busca completa. Verifique conexão com a internet e tente novamente.", "Ok");
                PecasListView.IsRefreshing = false;
                return;
            }

            if (ListaProdutos.Count < appData.QuantidadeProdutos) BuscaCompleta = true;

            foreach (var f in ListaProdutos) Produtos.Add(f);
            PecasListView.IsRefreshing = false;
        }

        private async Task<List<ProdutoModels>> BuscaFuncao(bool busca)
        {
            var appData = (AppData) BindingContext;
            PecasListView.IsVisible = true;
            try
            {
                ListaProdutos = await EnvioMethods.RetornaProdutoIndex(appData.CurrentOficina.Id, appData.BuscaString,
                    appData.QuantidadeProdutos, appData.ListaIndex, appData.Aplicacoes);

                return ListaProdutos;
            }
            catch
            {
                PecasListView.IsVisible = false;
                return new List<ProdutoModels>();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage) Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
            }
        }

        private async void IrPaginaCodigoBarras(object o, EventArgs e)
        {
            var scanPage = new ZXingScannerPage();

            scanPage.OnScanResult += result =>
            {
                while (result == null)
                {
                    scanPage.AutoFocus();
                    Task.Delay(1000);
                }

                // Stop scanning
                scanPage.IsScanning = false;
                // Pop the page and show the result
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PopAsync();
                    //DisplayAlert("Scanned Barcode", result.Text, "OK");
                    Busca.Text = result.Text;
                    BuscaPecas(new object(), new EventArgs());
                });
            };
            await Navigation.PushAsync(scanPage);
        }
    }
}