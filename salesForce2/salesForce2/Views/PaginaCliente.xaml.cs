﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaCliente : ContentPage
    {

        public PaginaCliente()
        {
            InitializeComponent();
            Title = "";
            Titulador();
            BuscaVeiculos();
            //VeiculosListView.IsRefreshing = false;
            StackLayoutEmail.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnEmailClicked()),
            });
            StackLayoutTelefone.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnTelClicked()),
            });
        }

        private void OnEmailClicked()
        {
            var appData = (AppData)BindingContext;
            Device.OpenUri(new Uri("mailto:"+appData.CurrentCliente.Email));
        }
        private void OnTelClicked()
        {
            var appData = (AppData)BindingContext;
            Device.OpenUri(new Uri("tel:"+appData.CurrentCliente.Ddd+appData.CurrentCliente.Celular));
        }

        public ObservableCollection<ClienteVeiculoModels> ListaVeiculos { get; set; }

        private void Titulador()
        {
            var appData = (AppData)BindingContext;
            if (string.IsNullOrEmpty(appData.BuscaString)) appData.BuscaString = appData.CurrentCliente.NomeFantasia;

            if (string.IsNullOrEmpty(appData.CurrentCliente.NomeFantasia)) return;
            var split = appData.CurrentCliente.NomeFantasia.Split(' ');
            var i = 0;
            var contador = split.Count();
            if (contador >= 5)
                while (i < 5 && Title.Length < 18)
                {
                    Title = Title + " " + split[i];
                    i++;
                }
            else
                while (i < contador && Title.Length < 15)
                    if (i > 2 && split[i].Length < 3)
                    {
                        i++;
                    }
                    else
                    {
                        Title = Title + " " + split[i];
                        i++;
                    }

            try
            {
                if (appData.CurrentCliente.Cep.Length == 8)
                    appData.CurrentCliente.Cep = appData.CurrentCliente.Cep.Substring(0, 5) + "-" +
                                                 appData.CurrentCliente.Cep.Substring(5, 7);
            }
            catch
            {
            }
        }

        private async void BuscaVeiculos()
        {
            var appData = (AppData)BindingContext;
            ListaVeiculos = new ObservableCollection<ClienteVeiculoModels>();
            try
            {
                VeiculosListView.IsRefreshing = true;
                var lista = await EnvioMethods.RetornarClienteVeiculos(appData.CurrentCliente.Id);
                VeiculosListView.IsRefreshing = false;
                VeiculosListView.IsVisible = true;
                foreach (var f in lista) ListaVeiculos.Add(f);
                VeiculosListView.ItemsSource = ListaVeiculos;
                VeiculosListView.HeightRequest = (VeiculosListView.RowHeight + 35) * ListaVeiculos.Count;
            }
            catch (Exception error)
            {
                VeiculosListView.IsRefreshing = false;
                await DisplayAlert("Erro", "Houve um erro na busca dos veículos.", "Ok");
            }

            VeiculosListView.IsRefreshing = false;
        }

        private async void AdicionarVeículo_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            appData.AdicionarCliente = false;
            await Navigation.PushAsync(new PaginaAdicionarVeiculo(appData.CurrentCliente));
        }

        private void VeiculoSelecionadoListView(object sender, SelectedItemChangedEventArgs item)
        {
            if (item.SelectedItem == null) return;
            VeiculosListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
            }
        }

        protected override void OnDisappearing()
        {
            var appData = (AppData)BindingContext;
            base.OnDisappearing();
            try
            {
                appData.CurrentCliente = new ClienteModels();
            }
            catch
            {
                //
            }
        }
    }
}