﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaTeste : ContentPage
    {
        public PaginaTeste()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //System.Diagnostics.Debug.WriteLine("aaaa");
        }

        public void Voltar()
        {
            Navigation.PopAsync(true);
        }
    }
}