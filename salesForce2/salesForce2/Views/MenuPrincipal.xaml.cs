﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPrincipal : ContentPage
    {
        public bool Clique;


        public MenuPrincipal()
        {
            InitializeComponent();
            CarregarNumeroOrcamento();
        }


        private async void BotaoConsultaTodasPecas(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            appData.Aplicacoes = false;
            if (Clique) return;
            Clique = true;
            await Navigation.PushAsync(new PaginaTodasPecas());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            //ImagensSwitch(true);
            Clique = false;
        }

        private async void BotaoAplicacoes(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            appData.Aplicacoes = true;

            if (Clique) return;
            Clique = true;
            await Navigation.PushAsync(new PaginaTodasPecas());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            //ImagensSwitch(true);
            Clique = false;
        }

        private async void BotaoConsultaTodosClientes(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            if (Clique) return;
            Clique = true;
            //ImagensSwitch(false);
            await Navigation.PushAsync(new PaginaTodosClientes());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            Clique = false;
            //ImagensSwitch(true);
        }

        private async void BotaoOrdemServico(object sender, EventArgs e)
        {
            //ImagensSwitch(false);
            if (Clique) return;

            Clique = true;

            var appData = (AppData) BindingContext;
            appData.Avarias = "";
            appData.Defeitos = "";
            appData.Observacao = "";
            appData.CurrentCliente = new ClienteModels();
            appData.CurrentOrdemServico = new OrdemServicoModels();
            await Navigation.PushAsync(new PaginaOrdemServico());
            Clique = false;
            //ImagensSwitch(true);
        }

        private async void CarregarNumeroOrcamento()
        {
            var appData = (AppData) BindingContext;

            try
            {
                var response =
                    await Client.Http.GetAsync("home/RetornarTodasOrdensServico?id=" + appData.CurrentOficina.Id);
                var resultado = await response.Content.ReadAsStringAsync();
                var listaOrdemServicos = JsonConvert.DeserializeObject<List<OrdemServicoModels>>(resultado);
                appData.NumeroOrcamento = listaOrdemServicos.Count;
            }
            catch (Exception)
            {
            }

            //Limpa dados desnecessarios
        }

        //private void ImagensSwitch(bool opcao)
        //{
        //    ImagemPecas.IsEnabled = opcao;
        //    ImagemAplicacoes.IsEnabled = opcao;
        //    ImagemClientes.IsEnabled = opcao;
        //    ImagemOrdemServico.IsEnabled = opcao;
        //}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage) Application.Current.MainPage).IsGestureEnabled = true;
            }
            catch (Exception error)
            {
                //DisplayAlert("Erro", error.Message, "Ok");
            }
        }
    }
}