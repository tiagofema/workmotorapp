﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaSelecaoOrdemServico : ContentPage
    {
        public PaginaSelecaoOrdemServico()
        {
            InitializeComponent();


        }

        private void BotaoConsultaOs_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PaginaConsultaOrdemServico());
        }

        private void BotaoAdicionarOs_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PaginaOrdemServico(false));
        }
    }
}