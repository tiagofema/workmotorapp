﻿using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaPeca : ContentPage
    {
        public PaginaPeca()
        {
            InitializeComponent();
            Title = "";
            Titulador();
        }

        private void Titulador()
        {
            var appData = (AppData) BindingContext;
            if (string.IsNullOrEmpty(appData.CurrentProduto.Descricao)) return;
            var split = appData.CurrentProduto.Descricao.Split(' ');
            var i = 0;
            var contador = split.Count();
            if (contador >= 5)
                while (i < 5 && Title.Length < 18)
                {
                    Title = Title + " " + split[i];
                    i++;
                }
            else
                while (i < contador && Title.Length < 15)
                    if (i > 2 && split[i].Length < 3)
                    {
                        i++;
                    }
                    else
                    {
                        Title = Title + " " + split[i];
                        i++;
                    }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage) Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
            }
        }
    }
}