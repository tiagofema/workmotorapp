﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaTodosClientes : ContentPage
    {
        public PaginaTodosClientes()
        {
            InitializeComponent();
        }

        public ObservableCollection<ClienteModels> Clientes { get; set; }
        private List<ClienteModels> ListaClientes { get; set; }
        public bool BuscaCompleta { get; set; }
        public bool Ocupado { get; set; }
        public bool AdicionarCliente { get; set; }

        private void ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var items = Clientes;
            if (BuscaCompleta) return;
            if (ClientesListView.IsRefreshing) return;

            if (items.Count < 10)
            {
                BuscaCompleta = true;
                return;
            }

            if (items != null && e.Item == items[items.Count - 5]) CarregarMaisClientes();
            ClientesListView.IsVisible = Clientes.Count > 0;
        }

        private void ListarClientes(List<ClienteModels> listaClientes)
        {
            //Produtos = new ObservableCollection<ProdutoViewModel>();
            Clientes = new ObservableCollection<ClienteModels>();

            FormataDocumento(listaClientes);
            ClientesListView.ItemsSource = Clientes;
            ClientesListView.IsVisible = Clientes.Count > 0;
        }


        private void ClienteSelecionadoListView(object sender, SelectedItemChangedEventArgs item)
        {
            var appData = (AppData)BindingContext;
            if (item.SelectedItem == null) return;
            ClientesListView.SelectedItem = null;
            appData.CurrentCliente = (ClienteModels)item.SelectedItem;
            IrPaginaClientes((ClienteModels)item.SelectedItem);
        }

        private async void IrPaginaClientes(ClienteModels cliente)
        {
            await Navigation.PushAsync(new PaginaCliente());
        }

        private async void BuscaClientes(object sender, EventArgs e)
        {
            if (Ocupado) return;
            Ocupado = true;
            var appData = (AppData)BindingContext;
            appData.CurrentCliente = new ClienteModels();
            appData.Busca = !string.IsNullOrEmpty(Busca.Text);
            appData.ListaIndex = 0;
            AdicionarClienteStack.IsVisible = false;
            try
            {
                appData.BuscaString = Regex.Replace(VerificadoresDocumentos.RemoverAcentos(Busca.Text),
                    "[^A-Za-z0-9\\s]", string.Empty);
            }
            catch
            {
                appData.BuscaString = "";
            }

            Clientes = new ObservableCollection<ClienteModels>();
            ClientesListView.ItemsSource = Clientes;
            ClientesListView.IsRefreshing = true;
            ClientesListView.IsVisible = true;
            await BuscaFuncao(appData.Busca);
            if (string.IsNullOrEmpty(appData.BuscaString)) appData.CurrentClienteList = ListaClientes;
            ListarClientes(ListaClientes);
            Ocupado = false;
            ClientesListView.IsRefreshing = false;
        }

        private async void CarregarMaisClientes()
        {
            var appData = (AppData)BindingContext;
            ClientesListView.IsRefreshing = true;
            appData.ListaIndex += appData.QuantidadeProdutos;

            try
            {
                await BuscaFuncao(appData.Busca);
            }
            catch
            {
                ClientesListView.IsRefreshing = false;
                return;
            }

            if (ListaClientes.Count < appData.QuantidadeProdutos) BuscaCompleta = true;

            FormataDocumento(ListaClientes);
            ClientesListView.IsRefreshing = false;
        }

        private async Task<List<ClienteModels>> BuscaFuncao(bool busca)
        {
            try
            {
                var appData = (AppData) BindingContext;
                var id = appData.CurrentOficina.Id;
                var buscaParam = appData.BuscaString;
                var qtd= appData.QuantidadeProdutos;
                var lista = appData.ListaIndex;
                if (Client.Http == null) return null;
                var response = await Client.Http.GetAsync("home/RetornarBuscaClienteIndex?id=" +
                                                          id +
                                                          "&descricao=" + buscaParam + "&quantidade=" + qtd
                                                           + "&index=" + lista);
                var resultado = await response.Content.ReadAsStringAsync();
                ListaClientes = JsonConvert.DeserializeObject<List<ClienteModels>>(resultado);
                foreach (var c in ListaClientes)
                    if (string.IsNullOrEmpty(c.NomeFantasia) && !string.IsNullOrEmpty(c.RazaoSocial))
                        c.NomeFantasia = c.RazaoSocial;
                return ListaClientes;
            }
            catch (Exception e)
            {
                return null;
            }
           
        }

        private async void FormataDocumento(List<ClienteModels> listaCliente)
        {
            //var appData = (AppData) BindingContext;
            foreach (var f in listaCliente)
            {
                switch (f.CnpjCpf.Length)
                {
                    case 11 when VerificadoresDocumentos.IsDigitsOnly(f.CnpjCpf):
                        f.CnpjCpf = Convert.ToUInt64(f.CnpjCpf).ToString(@"000\.000\.000\-00");
                        //f.Documento = "CPF:";
                        break;
                    case 14 when VerificadoresDocumentos.IsDigitsOnly(f.CnpjCpf):
                        f.CnpjCpf = Convert.ToUInt64(f.CnpjCpf).ToString(@"00\.000\.000/0000-00");
                        //f.Documento = "CNPJ:";
                        break;
                }

                switch (f.Telefone.Length)
                {
                    case 8:
                        f.Telefone = Convert.ToUInt64(f.Telefone).ToString(@"0000\-0000");
                        break;
                    case 9:
                        f.Telefone = Convert.ToUInt64(f.Telefone).ToString(@"0 0000\-0000");
                        break;
                }

                if (f.Telefone.Length < 9) f.Telefone = "Indisponível";

                if (f.Email.Length < 5) f.Email = "Indisponível";
                if (f.Cep.Length >= 6) f.Cep = $"{f.Cep.Substring(0, 5)}-{f.Cep.Substring(5)}";
            }

            foreach (var f in listaCliente)
            {
                if (string.IsNullOrEmpty(f.NomeFantasia)) f.NomeFantasia = f.RazaoSocial;
                Clientes.Add(f);
            }

            if (Clientes.Count >= 1) return;
            var resposta = await DisplayAlert("Aviso", "Cliente não encontrado.", "Adicionar cliente", "Voltar");
            ClientesListView.IsRefreshing = false;
            AdicionarClienteStack.IsVisible = true;
            if (!resposta) return;
            IrParaAdicionarNovoCliente(Busca.Text);
        }

        private async void ClienteNovaOs_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            var myButton = (Button)sender;
            var documento = myButton.CommandParameter.ToString();
            appData.CurrentCliente = Clientes.FirstOrDefault(x => x.Id == int.Parse(documento));
            Navigation.InsertPageBefore(new PaginaOrdemServico(), this);
            await Navigation.PopAsync().ConfigureAwait(false);
        }

        private async void IrParaAdicionarNovoCliente(string dado)
        {
            var appData = (AppData)BindingContext;
            appData.AdicionarCliente = true;
            Navigation.InsertPageBefore(string.IsNullOrEmpty(dado) ? new PaginaAdicionarCliente() : new PaginaAdicionarCliente(dado), this);
            await Navigation.PopAsync().ConfigureAwait(false);
        }
        private async void AdicionarClienteButton_OnClicked(object sender, EventArgs e)
        {
            IrParaAdicionarNovoCliente("");
        }
        private void NumeroDocumento_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var input = Busca?.Text;
            if (input?.Length == 0)
            {
                Busca.Keyboard = Keyboard.Default;
                return;
            }
            input = Regex.Replace(input ?? "", "[^a-zA-Zà-üÀ-Ü0-9\\s]", string.Empty);
            var apenasDigitos = new Regex(@"[^\d]");
            var digits = apenasDigitos.Replace(input.Substring(0, 1) ?? "", "");
            Busca.Keyboard = digits.Length > 0 ? Keyboard.Numeric : Keyboard.Default;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
                //
            }
        }
    }
}