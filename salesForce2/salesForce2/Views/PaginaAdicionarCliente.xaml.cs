﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaAdicionarCliente : ContentPage
    {
        public int MunicipioIbge;

        public PaginaAdicionarCliente()
        {
            InitializeComponent();
            CarregaClienteMegaLaudo();
            //InicializaCampo();
        }
        public PaginaAdicionarCliente(string dado)
        {
            InitializeComponent();
            CarregaClienteMegaLaudo();
            //dado = Regex.Replace(dado ?? "", "[^a-zA-Zà-üÀ-Ü0-9\\s]", string.Empty);
            var apenasDigitos = new Regex(@"[^\d]");
            var digits = apenasDigitos.Replace(dado.Substring(0, 1) ?? "", "");
            if (digits.Length > 0) NroDocumento.Text = dado;
            else Nome.Text = dado;
        }

        private async void CarregaClienteMegaLaudo()
        {
            var appData = (AppData)BindingContext;
            if (!appData.InsercaoClienteOs) return;
            CarregandoMegaLaudoSwitch(true);
            try
            {
                var a = await EnvioMethods.RetornoMegaLaudo(appData.CurrentVeiculoCliente.Placa);
                var clienteSimples = JsonConvert.DeserializeObject<ClienteSimplesModels>(a);
                Nome.Text = clienteSimples.Nome;
                NroDocumento.Text = clienteSimples.CpfCnpj;
                appData.CurrentVeiculoCliente.Placa = clienteSimples.Placa.ToUpper();
                appData.CurrentVeiculoCliente.Cor = clienteSimples.Cor.ToUpper();
                appData.CurrentVeiculoCliente.Descricao = clienteSimples.Modelo;
                appData.CurrentVeiculoCliente.Ano = clienteSimples.Ano;
            }
            catch(Exception e)
            {
                CarregandoMegaLaudoSwitch(false);
            }

            CarregandoMegaLaudoSwitch(false);
        }

        private async void Avancar_OnClicked(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            SalvandoClienteSwitch(true);
            //----------------------------------------
            //Nome.Text = "Tiago Ferraz Martins";
            //NroDocumento.Text = "37311225884";
            //Telefone.Text = "1938344187";
            //Numero.Text = "259";
            //Endereco.Text = "Rua João Tibiriçá Piratininga";
            //Complemento.Text = "";
            //Cep.Text = "13330-450";
            //Email.Text = "tfmartinfox@hotmail.com";
            //Cidade.Text = "Indaiatuba";
            //Bairro.Text = "Jardim Pau Preto";
            //Uf.Text = "SP";
            //NroInscricaoEstadual.Text = "2481035810";
            //Celular.Text = "19996750555";
            //DataNascimento.Text = "27/12/1993";
            //Sexo.Text = "M";
            //----------------------------------------
            if (string.IsNullOrEmpty(Cidade.Text)) Cidade.Text = CidadesPicker.SelectedItem.ToString();
            if (string.IsNullOrEmpty(Uf.Text)) Uf.Text = EstadosPicker.SelectedItem.ToString().Split('-')[0].Trim();

            try
            {
                if (string.IsNullOrEmpty(Nome.Text))
                {
                    await DisplayAlert("Erro", "Por favor preencha nome do cliente.", "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }

                appData.CurrentCliente = new ClienteModels();
                appData.CurrentCliente.RazaoSocial =
                    VerificadoresDocumentos.LimpezaGeral(VerificadoresDocumentos.RemoverAcentos(Nome.Text).ToUpper());
                appData.CurrentCliente.NomeFantasia =
                    VerificadoresDocumentos.LimpezaGeral(VerificadoresDocumentos.RemoverAcentos(Nome.Text).ToUpper());
            }
            catch
            {
                await DisplayAlert("Erro",
                    "O nome do cliente contém caracteres inválidos. Por favor digite-o somente com letras.", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            try
            {
                if (string.IsNullOrEmpty(DataNascimento.Text))
                {
                    await DisplayAlert("Erro", "Por favor digite a data de nascimento do cliente.", "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }

                appData.CurrentCliente.DataNascimento =
                    DateTime.ParseExact(DataNascimento.Text, "dd/MM/yyyy", null);
                //var dateTime = DateTime.ParseExact(DataNascimento.Text, "d",CultureInfo.CurrentCulture);
            }
            catch
            {
                //await DisplayAlert("Erro", "ME FALA O ERRO" + erro.Message, "Ok");
                await DisplayAlert("Erro",
                    "Problemas em reconhecer a data. Digite somente números e no formato DIA/MÊS/ANO", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            try
            {
                if (string.IsNullOrEmpty(Sexo.Text))
                {
                    await DisplayAlert("Erro", "Por favor escolha um sexo para o cliente", "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }

                appData.CurrentCliente.Sexo = Sexo.Text[0];
            }
            catch
            {
                await DisplayAlert("Erro", "Por favor escolha um sexo para o cliente e tente novamente.", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            try
            {
                if (VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text).Length != 11 &&
                    VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text).Length != 14 ||
                    string.IsNullOrEmpty(NroDocumento.Text))
                {
                    await DisplayAlert("Erro",
                        "O número do documento não contem a quantidade correta de digitos.", "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }
            }
            catch
            {
                await DisplayAlert("Erro", "O número do documento não pode ser deixado vazio.", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            if (NroDocumento.Text.Length == 18)
            {
                if (!VerificadoresDocumentos.IsCnpj(VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text)))
                {
                    await DisplayAlert("Erro", "O número de CNPJ é inválido. Verifique os digitos e tente novamente.",
                        "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }
            }
            else
            {
                if (!VerificadoresDocumentos.IsCpf(VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text)))
                {
                    await DisplayAlert("Erro", "O número de CPF é inválido. Verifique os digitos e tente novamente.",
                        "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }
            }

            appData.CurrentCliente.CnpjCpf = VerificadoresDocumentos.LimpezaNumeros(NroDocumento.Text);
            if (string.IsNullOrEmpty(NroInscricaoEstadual.Text) || NroInscricaoEstadual.Text.Length < 3)
            {
                await DisplayAlert("Erro", "O número de RG/Inscrição Estadual é obrigatório.", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            appData.CurrentCliente.InscricaoEstadual =
                VerificadoresDocumentos.LimpezaPlaca(NroInscricaoEstadual.Text.ToUpper());
            appData.CurrentCliente.OficinaId = appData.CurrentOficina.Id;
            try
            {
                appData.CurrentCliente.Email = Email.Text.ToUpper();
                if (Email.TextColor != Color.Default) throw new Exception();
            }
            catch
            {
                await DisplayAlert("Erro", "E-Mail inválido.", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            try
            {
                if (VerificadoresDocumentos.LimpezaNumeros(Telefone.Text).Length < 10)
                {
                    await DisplayAlert("Erro",
                        "Número de telefone inválido. Digite o DDD seguido pelo número da linha.", "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }

                appData.CurrentCliente.Ddd =
                    int.Parse(VerificadoresDocumentos.LimpezaNumeros(Telefone.Text).Substring(0, 2));
                appData.CurrentCliente.Telefone = VerificadoresDocumentos.LimpezaNumeros(Telefone.Text).Substring(2);
            }
            catch
            {
                await DisplayAlert("Erro", "Por favor, digite um número de telefone.", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            try
            {
                if (VerificadoresDocumentos.LimpezaNumeros(Celular.Text).Length < 10)
                {
                    await DisplayAlert("Erro",
                        "Número de celular inválido. Digite o DDD seguido pelo número da linha.", "Ok");
                    SalvandoClienteSwitch(false);
                    return;
                }

                appData.CurrentCliente.CelularDdd =
                    int.Parse(VerificadoresDocumentos.LimpezaNumeros(Celular.Text).Substring(0, 2));
                appData.CurrentCliente.Celular = VerificadoresDocumentos.LimpezaNumeros(Celular.Text).Substring(2);
            }
            catch
            {
                await DisplayAlert("Erro", "Por favor, digite um número de celular.", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            if (string.IsNullOrEmpty(Cep.Text) || Cep.Text.Length < 8)
            {
                await DisplayAlert("Erro", "Por favor, digite um CEP válido", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            if (string.IsNullOrEmpty(Endereco.Text))
            {
                await DisplayAlert("Erro", "Por favor, utilize a busca de CEP para localizar um endereço válido.",
                    "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            appData.CurrentCliente.Bairro = Bairro.Text.ToUpper();
            if (string.IsNullOrEmpty(Cidade.Text) || string.IsNullOrEmpty(Bairro.Text) || string.IsNullOrEmpty(Uf.Text))
                BuscaCep_OnClicked(new object(), new EventArgs());
            appData.CurrentCliente.Cep = VerificadoresDocumentos.LimpezaNumeros(Cep.Text);
            appData.CurrentCliente.Cidade =
                VerificadoresDocumentos.LimpezaGeral(
                    VerificadoresDocumentos.RemoverAcentos(Cidade.Text.ToUpper().Replace("\'", " ")));
            appData.CurrentCliente.Endereco =
                VerificadoresDocumentos.LimpezaGeral(
                    VerificadoresDocumentos.RemoverAcentos(Endereco.Text.ToUpper().Replace("\'", " ")));
            try
            {
                appData.CurrentCliente.Numero = int.Parse(VerificadoresDocumentos.LimpezaNumeros(Numero.Text));
            }
            catch
            {
                await DisplayAlert("Erro", "Digite somente números no campo número", "Ok");
                SalvandoClienteSwitch(false);
                return;
            }

            appData.CurrentCliente.Complemento = string.IsNullOrEmpty(Complemento.Text)
                ? "SEM COMPLEMENTO"
                : VerificadoresDocumentos.LimpezaGeral(Complemento.Text.ToUpper().Replace("\'", " "));
            appData.CurrentCliente.Estado = Uf.Text;
            appData.CurrentCliente.Codigo = 0;
            appData.CurrentCliente.MunicipioIbge = MunicipioIbge;
            appData.AdicionarCliente = true;
            if (!appData.InsercaoClienteOs)
            {
                var resposta = await DisplayAlert("", "Deseja adicionar um veículo para este cliente?", "Sim", "Não");
                if (resposta)
                {
                    await Navigation.PushAsync(new PaginaAdicionarVeiculo());
                    SalvandoClienteSwitch(false);
                    return;
                }
            }
            else
            {
                await Navigation.PushAsync(new PaginaAdicionarVeiculo());
                SalvandoClienteSwitch(false);
                return;
            }

            try
            {
                var resposta2 = await EnvioMethods.EnvioCliente(appData.CurrentCliente);
                if (resposta2 == "true")
                {
                    await DisplayAlert("Sucesso", "O cliente foi gravado com sucesso", "Ok");
                    appData.AdicionarCliente = false;
                    SalvandoClienteSwitch(false);
                    NavegacaoMethods.NavegaMenuPrincipal();
                }
                else
                {
                    appData.AdicionarCliente = false;
                    await DisplayAlert("Erro",
                        "Houve algum problema com a gravação do cliente. Verifique os dados e tente novamente.", "Ok");
                    SalvandoClienteSwitch(false);
                }
            }
            catch
            {
                await DisplayAlert("Erro",
                    "Houve um erro na gravação do cliente. Verifique sua conexão com a internet e tente novamente.",
                    "Ok");
                SalvandoClienteSwitch(false);
            }
        }

        private async void BuscaCep_OnClicked(object sender, EventArgs e)
        {
            //var appData = (AppData) BindingContext;
            if (string.IsNullOrEmpty(Cep.Text))
            {
                await DisplayAlert("Erro", "Para pesquisar insira um CEP.", "Ok");
                return;
            }
            CarregandoCepSwitch(true);
            try
            {
                var response = await Client.Endereco.GetAsync(Cep.Text + "/json");
                var resultado = await response.Content.ReadAsStringAsync();
                var endereco = JsonConvert.DeserializeObject<EnderecoModels>(resultado);
                if (string.IsNullOrEmpty(endereco.Erro))
                {
                    Endereco.Text = VerificadoresDocumentos.RemoverAcentos(endereco.Logradouro);
                    Bairro.Text = VerificadoresDocumentos.RemoverAcentos(endereco.Bairro);
                    Cidade.Text = VerificadoresDocumentos.RemoverAcentos(endereco.Localidade);
                    Uf.Text = endereco.Uf;
                    MunicipioIbge = int.Parse(VerificadoresDocumentos.LimpezaNumeros(endereco.Ibge));
                    if (!string.IsNullOrEmpty(Cidade.Text)) Cidade.IsEnabled = false;
                    if (!string.IsNullOrEmpty(Uf.Text)) Cidade.IsEnabled = false;

                    CarregandoCepSwitch(false);
                }
                else
                {
                    // ReSharper disable once PossibleNullReferenceException
                    await DisplayAlert("Erro", "CEP não encontrado. Verifique os numero e tente novamente.", "Ok");
                    CarregandoCepSwitch(false);
                }
            }
            catch
            {
                // ReSharper disable once PossibleNullReferenceException
                await DisplayAlert("CEP não encontrado", "O CEP digitado não foi encontrado. Preencha os valores manualmente.", "Ok");
                CarregandoCepSwitch(false);
            }
            if (string.IsNullOrEmpty(Cidade?.Text) && string.IsNullOrEmpty(Uf?.Text))
            {
                try
                {
                    var appData = (AppData)BindingContext;
                    if (appData.Estados == null || appData.Estados.Count == 0)
                    {
                        var responseT = await Client.Http.GetAsync("EstadoApi");
                        var resultadoT = await responseT.Content.ReadAsStringAsync();
                        appData.Estados = JsonConvert.DeserializeObject<List<EstadoModel>>(resultadoT);
                    }

                    foreach (var estado in appData.Estados)
                    {
                        EstadosPicker.Items.Add(estado.Uf + " - " + estado.Nome);
                    }

                }
                catch { }
                StackLayoutCidadeEstado.IsVisible = false;
                StackLayoutListCidadeEstado.IsVisible = true;

            }
            else
            {
                StackLayoutCidadeEstado.IsVisible = true;
                StackLayoutListCidadeEstado.IsVisible = false;
            }
            StackLayoutEnderecoInputs.IsVisible = true;
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
                //
            }
        }

        private async void Sexo_OnFocused(object sender, FocusEventArgs e)
        {
            SexoPicker.SelectedIndex = -1;
            Sexo.Unfocus();
            SexoPicker.Focus();
            while (SexoPicker.SelectedIndex == -1) await Task.Delay(25);
            Sexo.Text = SexoPicker.SelectedItem.ToString();
        }

        private void CarregandoCepSwitch(bool entry)
        {
            CarregandoCep.IsRunning = entry;
            CarregandoCep.IsVisible = entry;
        }

        private void CarregandoMegaLaudoSwitch(bool entry)
        {
            CarregandoMegaLaudoTexto.IsVisible = entry;
            CarregandoMegaLaudo.IsRunning = entry;
            CarregandoMegaLaudo.IsVisible = entry;
        }

        private void SalvandoClienteSwitch(bool entry)
        {
            SalvandoCliente.IsRunning = entry;
            SalvandoCliente.IsVisible = entry;
        }

        private void NroDocumento_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            NroInscricaoEstadual.Placeholder = e.NewTextValue.Length == 18 ? "INSCRIÇÃO ESTADUAL" : "RG";
        }

        private async void EstadosPicker_OnFocused(object sender, EventArgs eventArgs)
        {
            try
            {
                var item = (Renderers.CustomPicker)sender;
                var appData = (AppData)BindingContext;
                var estado = appData.Estados.FirstOrDefault(x => x.Uf.Contains(item.SelectedItem.ToString().Split('-')[0].Trim()));
                var resultadoT = await (await Client.Http.GetAsync("EstadoApi/Todos?id=" + estado.Id)).Content.ReadAsStringAsync();
                var cidades = JsonConvert.DeserializeObject<List<CidadeModel>>(resultadoT);
                foreach (var cidade in cidades)
                {
                    CidadesPicker.Items.Add(cidade.Nome);
                }
            }
            catch { }
        }

        private async void NroDocumento_OnUnfocused(object sender, FocusEventArgs e)
        {
            var appData = (AppData)BindingContext;
            try
            {
                var retorno = await EnvioMethods.RetornarClienteCpfCnpj(appData.CurrentOficina.Id, NroDocumento.Text);
                if (retorno != null)
                {
                    var tipo = "CPF";
                    if (NroDocumento.Text.Replace("-","").Replace(".","").Replace(".","").Length != 11) tipo = "CNPJ";
                    await DisplayAlert("Atenção", "O " + tipo + " já está cadastrado.", "Ok");
                    Avancar.IsEnabled = false;
                }
                else
                {
                    Avancar.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                Avancar.IsEnabled = true;
            }
        }
    }
}