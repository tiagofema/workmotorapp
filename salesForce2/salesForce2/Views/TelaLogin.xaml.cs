﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TelaLogin : ContentPage
    {
        public TelaLogin()
        {
            InitializeComponent();
            GetFuncionarios();
            NavigationPage.SetHasNavigationBar(this, false);
            Senha.Text = "";
        }

        private List<FuncionarioModels> ListaFuncionarios { get; set; }

        private async Task GetFuncionarios()
        {
            var appData = (AppData) BindingContext;
            var values = new Dictionary<string, string>
            {
                {"cnpj", appData.OficinaCnpj}
            };
            var content = new FormUrlEncodedContent(values);
            try
            {
                var response = await Client.Http.PostAsync("home/RetornoTodosPorOficina", content);
                var resultado = await response.Content.ReadAsStringAsync();
                ListaFuncionarios = JsonConvert.DeserializeObject<List<FuncionarioModels>>(resultado);


                FuncionarioPicker.Items.Clear();
                foreach (var f in ListaFuncionarios) FuncionarioPicker.Items.Add(f.Nome);
                FuncionarioPicker.IsEnabled = true;
            }
            catch
            {
                await DisplayAlert("Erro", "Houve um problema na busca dos funcionário. Tente novamente.", "Ok");
            }
        }

        private void LogarBotao(object sender, EventArgs e)
        {
            MenuPrincipal();
        }

        private async void MenuPrincipal()
        {
            var appData = (AppData) BindingContext;
            ActivitySwitch(true);

            try
            {
                var funcionarioIndex = FuncionarioPicker.SelectedIndex;
                if (funcionarioIndex < 0)
                {
                    await DisplayAlert("Alerta", "Nenhum funcionário selecionado", "Ok");
                    ActivitySwitch(false);
                    return;
                }

                var funcionario = ListaFuncionarios[funcionarioIndex];

                var values = new Dictionary<string, string>
                {
                    {"funcId", funcionario.Id.ToString()},
                    {"text", Senha.Text.ToUpper()}
                };

                var content = new FormUrlEncodedContent(values);
                var response2 = await Client.Http.PostAsync("home/LoginFuncionario", content);
                var resultado2 = await response2.Content.ReadAsStringAsync();

                switch (resultado2)
                {
                    case "FALSE":
                        await DisplayAlert("Erro", "Senha incorreta. Verifique os digitos e tente novamente.", "Ok");
                        ActivitySwitch(false);
                        return;
                    case "LOGIN MÁXIMO":
                        await DisplayAlert("Erro",
                            "O número máximo de logins para essa oficina foi alcançado. Por favor entre com um funcionário que já se encontra logado ou deslogue com outro para conseguir entrar.",
                            "Ok");
                        ActivitySwitch(false);
                        return;
                    case "ATIVO":
                        await DisplayAlert("Erro",
                            "O funcionário já se encontra ativo em outro dispositivo. Deslogue no outro dispositivo e tente novamente.",
                            "Ok");
                        ActivitySwitch(false);
                        return;
                    case "TRUE": break;
                    case "PROBLEMA BANCO":
                        await DisplayAlert("Erro",
                            "Houve algum erro ao tentar se conectar ao servidor. Por favor, verifique sua conexão com a internet e tente novamente",
                            "Ok");
                        ActivitySwitch(false);
                        return;
                    default:
                        await DisplayAlert("Erro",
                            "Houve algum problema conectando com o servidor. Por favor, verifique sua conexão com a internet e tente novamente.",
                            "Ok");
                        ActivitySwitch(false);
                        return;
                }

                appData.CurrentFuncionario = funcionario;
                var paginaFuncionario = new NavigationPage(new MenuPrincipal2())
                {
                    BarBackgroundColor = Color.FromHex("#223c74"),
                    BarTextColor = Color.White
                    //BarBackgroundColor =  Color.White,
                    //BarTextColor = Color.FromHex("#223c74")
                };
                var rootPage = new MasterDetailBase
                {
                    Master = new MenuLateral(),
                    Detail = paginaFuncionario
                };
                //--------------------------------------------------------
                appData.Logado = true;
                Application.Current.Properties["appData"] = JsonConvert.SerializeObject(appData);
                await Application.Current.SavePropertiesAsync();

                Navigation.InsertPageBefore(rootPage, this);
                ActivitySwitch(false);
                await Navigation.PopAsync().ConfigureAwait(false);
                // Adiciona o funcionario ao appCollection, para que ele fique disponível para acesso depois
            }
            catch (Exception e)
            {
                await DisplayAlert("Alerta", "Erro carregando a próxima página.", "ok");
                ActivitySwitch(false);
            }
        }

        private async void EscolhaFuncionário(object sender, FocusEventArgs e)
        {
            FuncionarioPicker.SelectedIndex = -1;
            FuncionarioPicker.Focus();
            Funcionario.Unfocus();
            while (FuncionarioPicker.SelectedIndex == -1) await Task.Delay(25);
            var funcionario = ListaFuncionarios[FuncionarioPicker.SelectedIndex];
            Funcionario.Text = funcionario.Nome;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage) Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
                //
            }
        }

        private void ActivitySwitch(bool estado)
        {
            Carregando.IsRunning = estado;
            Carregando.IsVisible = estado;
        }
    }
}