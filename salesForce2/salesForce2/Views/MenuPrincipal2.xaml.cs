﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPrincipal2 : ContentPage
    {
        public bool Clique;


        public MenuPrincipal2()
        {
            InitializeComponent();
            var appData = (AppData) BindingContext;
            CarregarNumeroOrcamento();
            Task.Run(async () => { appData.Combustiveis = await EnvioMethods.RetornaCombustiveis(); }).Wait();
        }

        private async Task VerificaOrdemServico()
        {
            var appData = (AppData) BindingContext;
            if (!appData.InsercaoClienteOs) return;
            appData.InsercaoClienteOs = false;
        }

        private async void BotaoConsultaTodasPecas(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            appData.Aplicacoes = false;
            if (Clique) return;
            Title = "Menu";
            Clique = true;
            await Navigation.PushAsync(new PaginaTodasPecas());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            Clique = false;
        }

        private async void BotaoAplicacoes(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            appData.Aplicacoes = true;
            if (Clique) return;
            Title = "Menu";
            Clique = true;
            await Navigation.PushAsync(new PaginaTodasPecas());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            Clique = false;
        }

        private async void BotaoConsultaTodosClientes(object sender, EventArgs e)
        {
            var appData = (AppData) BindingContext;
            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;
            appData.ListaIndex = 0;
            if (Clique) return;
            Clique = true;
            Title = "Menu";
            await Navigation.PushAsync(new PaginaTodosClientes());
            Activity.IsRunning = false;
            LoadingStack.IsVisible = false;
            Clique = false;
        }

        private async void BotaoOrdemServico(object sender, EventArgs e)
        {
            if (Clique) return;
            Clique = true;
            var appData = (AppData) BindingContext;
            appData.Avarias = "";
            appData.Defeitos = "";
            appData.Observacao = "";
            Title = "Menu";
            appData.CurrentCliente = new ClienteModels();
            appData.CurrentOrdemServico = new OrdemServicoModels();
            await Navigation.PushAsync(new PaginaSelecaoOrdemServico());
            Clique = false;
        }

        private async Task CarregarNumeroOrcamento()
        {
            var appData = (AppData) BindingContext;
            appData.Logado = true;
            appData.InsercaoClienteOs = false;
            appData.CurrentCliente = new ClienteModels();
            appData.CurrentClienteVeiculo = new List<ClienteVeiculoModels>();
            Application.Current.Properties["appData"] = JsonConvert.SerializeObject(appData);
            await Application.Current.SavePropertiesAsync();
            try
            {
                var values = new Dictionary<string, string>
                {
                    {"funcionarioId", appData.CurrentOficina.Id.ToString()}
                };
                var content = new FormUrlEncodedContent(values);
                var response = await Client.Http.PostAsync("home/RetornarTodasOrdensServico", content);
                var resultado = await response.Content.ReadAsStringAsync();
                var listaOrdemServicos = JsonConvert.DeserializeObject<List<OrdemServicoModels>>(resultado);
                appData.NumeroOrcamento = listaOrdemServicos.Count;
            }
            catch
            {
                //
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Title = "WorkMotorApp";
            var appData = (AppData) BindingContext;

            try
            {
                ((MasterDetailPage) Application.Current.MainPage).IsGestureEnabled = true;
            }
            catch
            {
                //
            }

            VerificaOrdemServico();
            CarregarNumeroOrcamento();
        }

        private async Task CarregaCombustiveis()
        {
        }

        private async void BotaoTeste_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new PaginaTeste());
            }
            catch
            {
                //
            }
        }

        private async void VideosBotao_OnClicked(object sender, EventArgs e)
        {
            if (Clique) return;
            Title = "Menu";
            Clique = true;
            await Navigation.PushAsync(new PaginaCanal());
            Clique = false;
        }

        private async void TesteIntegracao_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaPagamento());
        }
    }
}