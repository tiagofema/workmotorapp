﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaCanal : ContentPage
    {
        public PaginaCanal()
        {
            InitializeComponent();
        }

        private void WebView_OnGoForwardRequested(object sender, EventArgs e)
        {
            if (Browser.CanGoForward) Browser.GoForward();
        }

        private void WebView_OnGoBackRequested(object sender, EventArgs e)
        {
            if (Browser.CanGoBack)
                Browser.GoBack();
            else
                Navigation.PopAsync();
        }

        private void Browser_OnNavigating(object sender, WebNavigatingEventArgs e)
        {
            Progresso.IsVisible = true;
        }

        private void Browser_OnNavigated(object sender, WebNavigatedEventArgs e)
        {
            Progresso.IsVisible = false;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await Progresso.ProgressTo(0.9, 900, Easing.SpringIn);
        }
    }
}