﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [Preserve(AllMembers = true)]
    public partial class TelaLoginEmpresa : ContentPage
    {
        public TelaLoginEmpresa()
        {
            InitializeComponent();
            CnpjEmpresa.Placeholder = "CNPJ";
            Senha.Placeholder = "SENHA";
            var appData = (AppData) BindingContext;
            appData.Logado = false;
            NavigationPage.SetHasNavigationBar(this, false);
            switch (appData.BuscaString)
            {
                case "ATIVO":
                    DisplayAlert("Erro", "Esse usuário se encontra ativo em outro dispositivo. Você foi deslogado.",
                        "Ok");
                    break;
                case "INTERNET":
                    DisplayAlert("Erro", "Não foi possível conectar ao servidor.", "Ok");
                    break;
            }

            appData.BuscaString = "";
        }

        private OficinaModels Oficina { get; set; }

        private async void LogarBotao(object sender, EventArgs e)
        {
            await LogarEmpresa();
        }

        private async Task LogarEmpresa()
        {
            var appData = (AppData) BindingContext;
            var cnpj = CnpjEmpresa.Text;
            var senha = Senha.Text;
            if (string.IsNullOrEmpty(cnpj))
            {
                CnpjEmpresa.Text = "";
                CnpjEmpresa.Placeholder = "CNPJ";
                CnpjEmpresa.PlaceholderColor = Color.Red;
                if (!string.IsNullOrEmpty(senha)) return;
                Senha.Placeholder = "INFORME A SENHA";
                Senha.PlaceholderColor = Color.Red;
                Senha.IsPassword = false;
                return;
            }

            if (string.IsNullOrEmpty(senha))
            {
                Senha.Placeholder = "INFORME A SENHA";
                Senha.PlaceholderColor = Color.Red;
                Senha.IsPassword = false;
                return;
            }

            LoadingStack.IsVisible = true;
            Activity.IsRunning = true;

            try
            {
                var values = new Dictionary<string, string>
                {
                    {"cnpj", VerificadoresDocumentos.LimpezaNumeros(cnpj)},
                    {"text", senha}
                };

                var content = new FormUrlEncodedContent(values);
                var response2 = await Client.Http.PostAsync("home/LoginEmpresa", content);
                //----- Conexão com o Web-Service para pegar a oficina pelo CPF
                //var response = await Client.Http.GetAsync("home/RetornarOficina?cnpj=" + VerificadoresDocumentos.LimpezaNumeros(cnpj));
                var resultado2 = await response2.Content.ReadAsStringAsync();

                switch (resultado2)
                {
                    case "NÃO ATIVA":
                        LoadingStack.IsVisible = false;
                        await DisplayAlert("Erro", "Empresa não se encontra ativa para esse serviço.", "Ok");
                        return;
                    case "TRUE":

                        break;
                    case "FALSE":
                        await DisplayAlert("Erro", "A senha está incorreta. Tente Novamente.", "Ok");
                        LoadingStack.IsVisible = false;
                        return;
                    case "INVÁLIDA":
                        await DisplayAlert("Erro",
                            "A oficina não se encontra cadastrada no serviço. Verifique o CNPJ e tente novamente.",
                            "Ok");
                        LoadingStack.IsVisible = false;
                        return;
                    default:
                        LoadingStack.IsVisible = false;
                        await DisplayAlert("Erro",
                            "Houve algum erro na conexão com o servidor. Por favor, verifique sua conexão com a internet e tente novamente",
                            "Ok");
                        return;
                }

                values = new Dictionary<string, string>
                {
                    {"cnpj", VerificadoresDocumentos.LimpezaNumeros(cnpj)}
                };
                content = new FormUrlEncodedContent(values);

                var response = await Client.Http.PostAsync("home/RetornarOficina", content);
                var resultado = await response.Content.ReadAsStringAsync();
                Oficina = JsonConvert.DeserializeObject<OficinaModels>(resultado);
            }
            catch
            {
                await DisplayAlert("Erro",
                    "A conexão falhou. Verifique sua conexão com a internet e tente novamente", "OK");
                Activity.IsRunning = false;
                LoadingStack.IsVisible = false;
                return;
            }

            appData.OficinaCnpj = VerificadoresDocumentos.LimpezaNumeros(cnpj);
            appData.CurrentOficina = Oficina;
            LoadingStack.IsVisible = false;
            Activity.IsRunning = false;
            Navigation.InsertPageBefore(new TelaLogin(), this);
            await Navigation.PopAsync().ConfigureAwait(false);
        }

        private void MudaCampo(object sender, EventArgs e)
        {
            CnpjEmpresa.Unfocus();
            Senha.Focus();
        }

        private void Senha_OnFocused(object sender, FocusEventArgs e)
        {
            Senha.IsPassword = true;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage) Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
            }
        }
    }
}