﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkmotorApp.Http;
using WorkmotorApp.Methods;
using WorkmotorApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkmotorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaOrdemServico : ContentPage
    {
        public PaginaOrdemServico(bool comCliente)
        {
            InitializeComponent();
            var appData = (AppData)BindingContext;
            try
            {
                if (appData.Combustiveis[0] == null) throw new Exception();
                ListaCombustiveis = appData.Combustiveis;
                foreach (var c in ListaCombustiveis) CombustivelPicker.Items.Add(c.Tipo.ToUpper());
                CombustivelPicker.IsEnabled = true;
            }
            catch
            {
                while (true)
                {
                    Task.Run(async () => { await GetCombustivel(); }).Wait();
                    if (ListaCombustiveis?[0] != null) break;
                }
            }

            appData.NumeroDocumento = "Nro.: Documento:";
            if (comCliente) GetClientes();
            if(!string.IsNullOrEmpty(NumeroDocumento.Text))NumeroDocumento_OnCompleted(new object(), new EventArgs());

        }
        public PaginaOrdemServico()
        {
            InitializeComponent();
            var appData = (AppData)BindingContext;
            try
            {
                if (appData.Combustiveis[0] == null) throw new Exception();
                ListaCombustiveis = appData.Combustiveis;
                foreach (var c in ListaCombustiveis) CombustivelPicker.Items.Add(c.Tipo.ToUpper());
                CombustivelPicker.IsEnabled = true;
            }
            catch
            {
                while (true)
                {
                    Task.Run(async () => { await GetCombustivel(); }).Wait();
                    if (ListaCombustiveis?[0] != null) break;
                }
            }

            appData.NumeroDocumento = "Nro.: Documento:";
            GetClientes();
            if(!string.IsNullOrEmpty(NumeroDocumento.Text))NumeroDocumento_OnCompleted(new object(), new EventArgs());
        }

        private ClienteModels Clientes { get; set; }
        private List<ClienteVeiculoModels> CarrosCliente { get; set; }
        private List<CombustivelModels> ListaCombustiveis { get; set; }
        private List<CarroFipeModels> ListaCarroFipe { get; set; }
        private CarroFipeModels CarroFipe { get; set; }
        private CombustivelModels Combustivel { get; set; }
        private bool CarregandoCarroListView { get; set; }
        private bool CarregandoCliente { get; set; }
        private bool Gravando { get; set; }


        private async void Gravar(object sender, EventArgs e)
        {
            if (Gravando) return;

            CarregandoGravacaoOnOff(true);
            var appData = (AppData)BindingContext;
            appData.CurrentOrdemServico.NumeroOrcamento = appData.NumeroOrcamento;
            appData.CurrentOrdemServico.OficinaId = appData.CurrentOficina.Id;

            try
            {
                var a = Nome.Text;
                var b = NumeroDocumento.Text;
                appData.CurrentOrdemServico.ClienteId = appData.CurrentCliente.Id;
            }
            catch
            {
                await DisplayAlert("Alerta",
                    "Nenhum cliente selecionado. Por favor digite um CPF/CNPJ e tente novamente.", "Ok");
                CarregandoGravacaoOnOff(false);
                return;
            }

            if (appData.CurrentCliente.Id == 0 && !string.IsNullOrEmpty(NumeroDocumento.Text))
                try
                {
                    //var documento = Regex.Replace(NumeroDocumento.Text, "[^0-9]", string.Empty);
                    Clientes = await EnvioMethods.RetornarClienteCpfCnpjPlaca(appData.CurrentOficina.Id, NumeroDocumento.Text);
                    Nome.Text = Clientes.NomeFantasia;
                    appData.CurrentCliente = Clientes;
                    appData.CurrentOrdemServico.ClienteId = appData.CurrentCliente.Id;
                }
                catch
                {
                    await DisplayAlert("Alerta", "Nenhum cliente selecionado. Por favor selecione um para continuar.",
                        "Ok");
                    CarregandoGravacaoOnOff(false);
                    return;
                }

            try
            {
                appData.CurrentOrdemServico.CarroFipeId = CarroFipe.Id;
            }
            catch
            {
                await DisplayAlert("Alerta",
                    "Nenhum Veículo selecionado. Por favor busque um veículo e o selecione na lista e tente novamente",
                    "Ok");
                CarregandoGravacaoOnOff(false);

                return;
            }

            if (int.TryParse(AnoEntry.Text, out var ano))
            {
                appData.CurrentOrdemServico.Ano = ano;
            }
            else
            {
                await DisplayAlert("Alerta", "O ano digitado é inválido. Digite no formato AAAA.", "Ok");
                AnoEntry.Focus();
                CarregandoGravacaoOnOff(false);
                return;
            }

            if (decimal.TryParse(Km.Text, out var quilometragem))
            {
                appData.CurrentOrdemServico.Quilometragem = quilometragem;
            }
            else
            {
                await DisplayAlert("Alerta",
                    "A quilometragem digitada é inválida. Digite no formato xxxx,xxx ou somente XXXXX .", "Ok");
                Km.Focus();
                CarregandoGravacaoOnOff(false);
                return;
            }

            appData.CurrentOrdemServico.FuncionarioId = appData.CurrentFuncionario.Id;
            var combustivelIndex = CombustivelPicker.SelectedIndex;
            try
            {
                if (combustivelIndex < 0 || Combustivel.Id == -1)
                {
                    await DisplayAlert("Alerta", "Nenhum combustível selecionado.", "Ok");
                    CombustivelPicker.Focus();
                    CarregandoGravacaoOnOff(false);
                    return;
                }
            }
            catch
            {
                await DisplayAlert("Alerta", "Houve algum problema carregando o combustível. Tente novamente.", "Ok");
                CarregandoGravacaoOnOff(false);
                return;
            }

            appData.CurrentOrdemServico.CombustivelId = Combustivel.Id;
            appData.CurrentOrdemServico.Avarias =
                string.IsNullOrEmpty(appData.Avarias)
                    ? ""
                    : VerificadoresDocumentos.LimpezaGeral(
                        VerificadoresDocumentos.RemoverAcentos(appData.Avarias.ToUpper()));
            appData.CurrentOrdemServico.Defeitos =
                string.IsNullOrEmpty(appData.Defeitos)
                    ? ""
                    : VerificadoresDocumentos.LimpezaGeral(
                        VerificadoresDocumentos.RemoverAcentos(appData.Defeitos.ToUpper()));
            appData.CurrentOrdemServico.Observacao =
                string.IsNullOrEmpty(appData.Observacao)
                    ? ""
                    : VerificadoresDocumentos.LimpezaGeral(
                        VerificadoresDocumentos.RemoverAcentos(appData.Observacao.ToUpper()));
            try
            {
                if (PlacaEntry.Text.Length < 8)
                {
                    await DisplayAlert("Alerta",
                        "Quantidade de digitos inválida no campo placa. O formato é AAA-0000. Tente novamente.", "Ok");
                    return;
                }
            }
            catch
            {
                await DisplayAlert("Alerta", "Por favor digite uma placa válida.", "Ok");
                return;
            }

            appData.CurrentOrdemServico.Placa = VerificadoresDocumentos.LimpezaPlaca(PlacaEntry.Text.ToUpper());
            appData.CurrentOrdemServico.DataEmissao = DateTime.Now;
            appData.CurrentOrdemServico.WorkMotorId = 0;
            appData.CurrentOrdemServico.Cor = string.IsNullOrEmpty(CorEntry.Text)
                ? "NÃO DECLARADA"
                : VerificadoresDocumentos.LimpezaGeral(CorEntry.Text.ToUpper());

            try
            {
                if (CarrosCliente.All(c =>
                    VerificadoresDocumentos.LimpezaPlaca(c.Placa) !=
                    VerificadoresDocumentos.LimpezaPlaca(appData.CurrentOrdemServico.Placa)))
                {
                    var carroCliente = new ClienteVeiculoModels
                    {
                        Ano = AnoEntry.Text,
                        Placa = VerificadoresDocumentos.LimpezaPlaca(PlacaEntry.Text.ToUpper()),
                        CarroFipeId = appData.CurrentOrdemServico.CarroFipeId,
                        ClienteId = appData.CurrentCliente.Id,
                        CombustivelId = Combustivel.Id,
                        CombustivelTipo = Combustivel.Tipo,
                        Cor = CorEntry.Text.ToUpper(),
                        Descricao = CarroFipe.Descricao,
                        Quilometragem = decimal.Parse(Km.Text)
                    };
                    try
                    {
                        var resultBool = await InsereVeiculo(carroCliente);

                        if (!resultBool)
                        {
                            await DisplayAlert("Alerta",
                                "Não foi possível adicionar o veículo ao cliente. Verifique sua conexão com a internet e tente novamente.",
                                "Okay");
                            CarregandoGravacaoOnOff(false);
                            return;
                        }
                    }
                    catch
                    {
                        CarregandoGravacaoOnOff(false);
                        return;
                    }
                }
            }
            catch
            {
                await DisplayAlert("Alerta",
                    "Houve um erro ao tentar inserir o veículo. Verifique os dados e tente novamente.", "Ok");
                CarregandoGravacaoOnOff(false);
                return;
            }

            appData.CurrentOrdemServico.Status = "Indefinido";
            appData.CurrentOrdemServico.TotalPecas = 0;
            appData.CurrentOrdemServico.TotalServicos = 0;
            appData.CurrentOrdemServico.Total = 0;
            appData.CurrentOrdemServico.DescProdutos = 0;
            appData.CurrentOrdemServico.DescServicos = 0;

            var result = await EnvioMethods.RecebeOrdemServico(appData.CurrentOrdemServico);

            if (result.ToUpper() == "TRUE")
            {
                await DisplayAlert("Sucesso",
                    "A ordem de serviço foi enviada para o WorkMotor.", "Ok");
                appData.NumeroOrcamento++;
                appData.Avarias = "";
                appData.Defeitos = "";
                appData.Observacao = "";
                CarregandoGravacaoOnOff(false);


                appData.CurrentCliente = new ClienteModels();
                appData.CurrentClienteVeiculo = new List<ClienteVeiculoModels>();
                NavegacaoMethods.NavegaMenuPrincipal();
                return;
            }

            await DisplayAlert("Alerta",
                "Houve um ou mais problemas na inserção. Verifique os dados e tente novamente.", "Ok");

            CarregandoGravacaoOnOff(false);
        }

        private async void NumeroDocumento_OnCompleted(object sender, EventArgs e)
        {
            var appData = (AppData)BindingContext;
            if (CarregandoCliente) return;
            CarregandoPesquisa.IsRunning = true;
            CarregandoPesquisa.IsVisible = true;
            CarregandoCliente = true;
            foreach (var c in CarroPicker.Items.ToList()) CarroPicker.Items.Remove(c);
            CarregandoGravacaoOnOff(true);
            string documento;
            try
            {
                documento = NumeroDocumento.Text.Length > 7
                    ? Regex.Replace(NumeroDocumento.Text, "[^0-9]", string.Empty)
                    : NumeroDocumento.Text;
            }
            catch
            {
                if (string.IsNullOrEmpty(NumeroDocumento.Text))
                {
                    await DisplayAlert("Alerta", "Nenhum CPF/CNPJ/PLACA a ser pesquisado. Preencha o campo e tente novamente.", "Ok");
                    CarregandoCliente = false;
                    CarregandoPesquisa.IsRunning = false;
                    CarregandoPesquisa.IsVisible = false;
                    CarregandoGravacaoOnOff(false);
                    return;
                }
                documento = NumeroDocumento.Text;
            }

            try
            {
                Clientes = await EnvioMethods.RetornarClienteCpfCnpjPlaca(appData.CurrentOficina.Id, documento);
                Nome.Text = Clientes.NomeFantasia;
                appData.CurrentCliente = Clientes;
            }
            catch
            {
                var resposta = await DisplayAlert(
                    "Alerta", "Não foi encontrado o cliente com esse CPF/CNPJ/PLACA.", "Adicionar Cliente", "Voltar");
                Nome.Text = " NOME DO CLIENTE ";
                CarregandoPesquisa.IsRunning = false;
                CarregandoPesquisa.IsVisible = false;
                CarregandoCliente = false;
                CarregandoGravacaoOnOff(false);
                if (!resposta) return;
                if(documento.Length <= 7)
                {
                    appData.CurrentVeiculoCliente = new ClienteVeiculoModels()
                    {
                        Placa =documento
                    };
                }
                else
                {
                    appData.CurrentCliente = new ClienteModels
                    {
                        CnpjCpf = documento
                    };
                }
                appData.InsercaoClienteOs = true;
               
                CarregandoGravacaoOnOff(false);
                CarregandoPesquisa.IsRunning = false;
                CarregandoPesquisa.IsVisible = false;
                CarregandoCliente = false;
                Navigation.InsertPageBefore(new PaginaAdicionarCliente(), this);
                await Navigation.PopAsync().ConfigureAwait(false);
            }
            if(documento.Length > 8) await CarregaVeiculos("");
            else await CarregaVeiculos(documento);
            CarregandoPesquisa.IsRunning = false;
            CarregandoPesquisa.IsVisible = false;
            CarregandoCliente = false;
            CarregandoGravacaoOnOff(false);
        }

        private void BuscaDocumento_OnClicked(object sender, EventArgs e)
        {
            NumeroDocumento_OnCompleted(new object(), new EventArgs());
        }

        private async void GetVeiculos(object sender, EventArgs e)
        {
            await CarregaVeiculos("");
        }
     
        private async Task EscolheVeiculo()
        {
            var appData = (AppData)BindingContext;
            CarroPicker.SelectedIndex = -1;
            CarroPicker.Focus();
            Veiculo.Unfocus();
            while (CarroPicker.SelectedIndex == -1) await Task.Delay(25);
            var carro = ListaCarroFipe[CarroPicker.SelectedIndex];
            Veiculo.Text = carro.Descricao;
            CarroFipe = carro;
            appData.CurrentOrdemServico.CarroFipeId = carro.Id;
            StackLayoutDados.IsVisible = true;
        }

        private void Ano_OnCompleted(object sender, EventArgs e)
        {
            Km.Focus();
        }

        private void Km_OnCompleted(object sender, EventArgs e)
        {
            CombustivelPicker.Focus();
        }

        private async Task GetClientes()
        {
            var appData = (AppData)BindingContext;
            appData.InsercaoClienteOs = false;
            CarregandoGravacaoOnOff(false);
            try
            {
                Nome.Text = appData.CurrentCliente.NomeFantasia;
                NumeroDocumento.Text = appData.CurrentCliente.CnpjCpf;
                appData.CurrentOrdemServico = new OrdemServicoModels();
                await CarregaVeiculos("");
                if (!string.IsNullOrEmpty(appData.CurrentCliente.NomeFantasia)) return;
                Nome.Text = "NOME DO CLIENTE";
                //NumeroDocumento.Placeholder = "      CPF/CNPJ         ";
                appData.CurrentOrdemServico.ClienteId = appData.CurrentCliente.Id;
            }
            catch
            {
                //   
            }
        }

        private async void BotaoDetalhes_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaObservacoes());
        }

        private async void CombustivelEntry_OnFocused(object sender, FocusEventArgs e)
        {
            CombustivelPicker.SelectedIndex = -1;
            CombustivelPicker.Focus();
            CombustivelEntry.Unfocus();
            while (CombustivelPicker.SelectedIndex == -1) await Task.Delay(25);
            Combustivel = ListaCombustiveis[CombustivelPicker.SelectedIndex];
            CombustivelEntry.Text = Combustivel.Tipo.ToUpper();
        }

        private async Task GetCombustivel()
        {
            try
            {
                var response = await Client.Http.GetAsync("home/RetornarTodosCombustiveis");
                var resultado = await response.Content.ReadAsStringAsync();
                ListaCombustiveis = JsonConvert.DeserializeObject<List<CombustivelModels>>(resultado);
            }
            catch
            {
                await DisplayAlert("Alerta",
                    "Não foi possivel conectar-se ao servidor. Verifique sua conexão com a internet e tente novamente.",
                    "Ok");
            }

            foreach (var c in ListaCombustiveis) CombustivelPicker.Items.Add(c.Tipo.ToUpper());
            CombustivelPicker.IsEnabled = true;
        }

        private void CarregandoGravacaoOnOff(bool ativo)
        {
            Gravando = ativo;
            CarregandoGravacao.IsRunning = ativo;
            CarregandoGravacao.IsVisible = ativo;
        }

        private async Task CarregaVeiculos(string placa)
        {
            CarregandoGravacaoOnOff(true);
            var appData = (AppData)BindingContext;
            try
            {
                if (appData.CurrentCliente.Id == 0)
                {
                    CarregandoGravacaoOnOff(false);
                    return;
                }
                if (CarroPicker?.Items?.Count == 0)
                {
                    CarrosCliente = new List<ClienteVeiculoModels>();
                    var values2 = new Dictionary<string, string>
                    {
                        {"documento", appData.CurrentCliente.Id.ToString()}
                    };
                    var content2 = new FormUrlEncodedContent(values2);
                    var response = await Client.Http.PostAsync("home/RetornarClienteVeiculos", content2);
                    var resultado = await response.Content.ReadAsStringAsync();
                    CarrosCliente = JsonConvert.DeserializeObject<List<ClienteVeiculoModels>>(resultado);
                    if (!string.IsNullOrEmpty(placa)) CarrosCliente = CarrosCliente.Where(x => x.Placa == placa?.Replace("-", "").ToUpper()).ToList();
                }

                try
                {
                    if (CarroPicker?.Items?.Count == 0)
                    {
                        //[POST]
                        var values = new Dictionary<string, string>
                        {
                            {"carrosCliente", JsonConvert.SerializeObject(CarrosCliente)}
                        };

                        var content = new FormUrlEncodedContent(values);
                        var response2 = await Client.Http.PostAsync("home/RetornaCarrosFipeCliente", content);
                        var responseString = await response2.Content.ReadAsStringAsync();
                        ListaCarroFipe = JsonConvert.DeserializeObject<List<CarroFipeModels>>(responseString);

                        if (CarrosCliente == null || CarrosCliente.Count == 0)
                        {
                            await DisplayAlert("Atenção", "Cliente não contém veículo cadastrado.", "Ok");
                            CarregandoGravacaoOnOff(false);
                            return;
                        }
                    }

                    if (CarrosCliente.Count == 1)
                    {
                        appData.CurrentOrdemServico.CarroFipeId = CarrosCliente[0].CarroFipeId;
                        CarroFipe = ListaCarroFipe.First(c1 => c1.Id == CarrosCliente[0].CarroFipeId);
                    }
                    else if (CarrosCliente.Count > 1)
                    {
                        if (CarroPicker.Items.Count == 0)
                        {
                            ListaCarroFipe = new List<CarroFipeModels>(ListaCarroFipe.OrderBy(c => c.Descricao));
                            foreach (var f in ListaCarroFipe) CarroPicker.Items.Add(f.Descricao);
                        }

                        await EscolheVeiculo();
                    }

                    CarregandoGravacaoOnOff(false);
                    var carroCliente = CarrosCliente.First(c1 => c1.CarroFipeId == appData.CurrentOrdemServico.CarroFipeId);
                    Veiculo.Text = carroCliente.Descricao.Length > 30
                        ? carroCliente.Descricao.Substring(0, 30)
                        : carroCliente.Descricao;
                    AnoEntry.Text = carroCliente.Ano;
                    Km.Text = carroCliente.Quilometragem.ToString();
                    CombustivelEntry.Text = carroCliente.CombustivelTipo;
                    CorEntry.Text = carroCliente.Cor;
                    Combustivel = new CombustivelModels
                    {
                        Id = carroCliente.CombustivelId,
                        Tipo = carroCliente.CombustivelTipo
                    };
                    appData.CurrentOrdemServico.CombustivelId = carroCliente.CombustivelId;
                    PlacaEntry.Text = carroCliente.Placa;
                    CombustivelPicker.SelectedIndex = 1;
                    StackLayoutDados.IsVisible = true;
                    if (CarrosCliente.Count == 1)
                    {
                        BuscaVeiculo.IsEnabled = false;
                        BuscaVeiculo.BackgroundColor = Color.FromHex("#404e6d");
                    }

                }
                catch(Exception e)
                {
                    await DisplayAlert("Erro",
                        "Houve um erro buscando os veículos. Verifique sua conexão com a internet e tente novamente.",
                        "Ok");
                    CarregandoGravacaoOnOff(false);
                }
            }
            catch (Exception e)
            {
                await DisplayAlert("Erro",
                    "Houve um erro buscando os veículos. Verifique sua conexão com a internet e tente novamente. ",
                    "Ok");
                CarregandoGravacaoOnOff(false);


            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                ((MasterDetailPage)Application.Current.MainPage).IsGestureEnabled = false;
            }
            catch
            {
                //
            }
        }

        private async void Veiculo_OnFocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (Veiculo.Text.Length > 0) return;
            }
            catch
            {
                return;
            }

            try
            {
                if (Veiculo.Text.Length < 2) return;
            }
            catch
            {
                return;
            }

            try
            {
                ListaCarroFipe = new List<CarroFipeModels>();
                if (CarrosCliente.Count > 0) await EscolheVeiculo();
            }
            catch
            {
                //
            }
        }

        private async Task<bool> InsereVeiculo(ClienteVeiculoModels carroCliente)
        {
            try
            {
                var veiculoSerializado1 = JsonConvert.SerializeObject(carroCliente);
                var result2 = await EnvioMethods.AdicionarVeiculoCliente(veiculoSerializado1);

                if (result2.ToUpper() == "TRUE") return true;
                await DisplayAlert("Erro",
                    "Não foi possivel fazer a inclusão do veículo. Verifique se os dados estão corretos e tente novamente.",
                    "Ok");
                return false;
            }
            catch
            {
                await DisplayAlert("Erro",
                    "Houve algum problema na inserção. Verifique os dados e sua conexão com a internet e tente novamente.",
                    "Ok");
                return false;
            }
        }

        private void NumeroDocumento_OnTextChanged(object sender, TextChangedEventArgs e)
        {

            var input = NumeroDocumento?.Text;
            if (input?.Length == 0)
            {
                NumeroDocumento.Keyboard = Keyboard.Default;
                return;
            }
            input = Regex.Replace(input ?? "", "[^a-zA-Zà-üÀ-Ü0-9\\s]", string.Empty);
            var apenasDigitos = new Regex(@"[^\d]");
            var digits = apenasDigitos.Replace(input.Substring(0, 1) ?? "", "");
            if (digits.Length > 0)
            {
                NumeroDocumento.Keyboard = Keyboard.Numeric;
            }
            else
            {
                var contagem = input.ToUpper()?.Cast<char>().Count(char.IsUpper);
                NumeroDocumento.Keyboard = contagem == 3 ? Keyboard.Numeric : Keyboard.Default;
            }
        }


        private void CarroPicker_OnUnfocused(object sender, FocusEventArgs e)
        {
            if (CarroPicker.SelectedIndex == -1)
            {
                CarregandoCliente = false;
                CarregandoPesquisa.IsRunning = false;
                CarregandoPesquisa.IsVisible = false;
            }
        }
    }
}