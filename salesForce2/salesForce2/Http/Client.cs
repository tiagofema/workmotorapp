﻿using System;
using System.Net.Http;

namespace WorkmotorApp.Http
{
    public static class Client
    {
        //public static HttpClient Http => new HttpClient {BaseAddress = new Uri("http://192.168.0.115/")};
        //public static HttpClient Http => new HttpClient { BaseAddress = new Uri("http://192.168.0.103:62838/") };
        //public static HttpClient Http => new HttpClient {BaseAddress = new Uri("http://201.77.177.34/")};
        public static HttpClient Http => new HttpClient {BaseAddress = new Uri("http://187.72.202.129/") };

        //public static HttpClient Endereco => new HttpClient {BaseAddress = new Uri("http://api.postmon.com.br/v1/cep/")};
        public static HttpClient Endereco => new HttpClient {BaseAddress = new Uri("https://viacep.com.br/ws/")};

        public static HttpClient MegaLaudo => new HttpClient
        {
            BaseAddress = new Uri("http://201.77.177.42/ApiMegaLaudo/")
        };
    }
}