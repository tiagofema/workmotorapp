﻿namespace WorkmotorApp.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            LoadApplication(new WorkmotorApp.App());
        }
    }
}