﻿using Android.App;
using Android.OS;

namespace WorkmotorApp.Droid
{
    [Activity(Theme = "@style/Theme.Splash",
        Icon = "@drawable/workmotorapplaucher",
        MainLauncher = true,
        NoHistory = false)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            StartActivity(typeof(MainActivity));
            //Task.Delay(3000);
            Finish();
            // Create your application here
        }
    }
}