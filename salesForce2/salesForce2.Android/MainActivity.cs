﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ZXing.Net.Mobile.Forms.Android;
using Platform = ZXing.Net.Mobile.Forms.Android.Platform;
//SENHA PARA PUBLICAR NA GOOGLE PLAY workg9999
namespace WorkmotorApp.Droid
{
    [IntentFilter(new[] {Intent.ActionView},
        Categories = new[] {Intent.CategoryBrowsable, Intent.CategoryDefault},
        DataScheme = "http",
        DataHost = "workmotor.com.br",
        DataPathPrefix = "/sucesso",
        AutoVerify = false)]

    //[IntentFilter(new[] { Android.Content.Intent.ActionView },
    //    Categories = new[] { Android.Content.Intent.CategoryBrowsable, Android.Content.Intent.CategoryDefault },
    //    DataScheme = "http",
    //    DataHost = "workmotor.com.br",
    //    DataPathPrefix = "/fracasso",
    //    AutoVerify = false)]
    [Activity(Label = "WorkMotor", Icon = "@drawable/workmotorapplaucher", Theme = "@style/MainTheme",
        MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.FullUser, WindowSoftInputMode = SoftInput.AdjustResize)]
    public class MainActivity : FormsAppCompatActivity //FormsAppCompatActivity   FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);
            Forms.Init(this, bundle);
            //--------------------------------------------------------
            //var uiOptions = (int)Window.DecorView.SystemUiVisibility;

            //uiOptions |= (int)SystemUiFlags.LowProfile;
            //uiOptions |= (int)SystemUiFlags.Fullscreen;
            //uiOptions |= (int)SystemUiFlags.HideNavigation;
            //uiOptions |= (int)SystemUiFlags.ImmersiveSticky;

            //Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
            //------------------------------------------------------------------

            //Adicionando o ZXing para leitura de código de barras
            Platform.Init();

            LoadApplication(new App());
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            Permission[] grantResults)
        {
            PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        //protected override void OnStart()
        //{
        //    base.OnStart();
        //    var uiOptions = (int)Window.DecorView.SystemUiVisibility;

        //    uiOptions |= (int)SystemUiFlags.LowProfile;
        //    uiOptions |= (int)SystemUiFlags.Fullscreen;
        //    uiOptions |= (int)SystemUiFlags.HideNavigation;
        //    uiOptions |= (int)SystemUiFlags.ImmersiveSticky;

        //    Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;

        //}

        //protected override void OnResume()
        //{
        //    base.OnResume();
        //    var uiOptions = (int)Window.DecorView.SystemUiVisibility;

        //    uiOptions |= (int)SystemUiFlags.LowProfile;
        //    uiOptions |= (int)SystemUiFlags.Fullscreen;
        //    uiOptions |= (int)SystemUiFlags.HideNavigation;
        //    uiOptions |= (int)SystemUiFlags.ImmersiveSticky;
        //    Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
        //}
    }
}