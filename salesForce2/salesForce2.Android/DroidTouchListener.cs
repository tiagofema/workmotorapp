﻿using Android.Views;
using Java.Lang;

namespace WorkmotorApp.Droid
{
    public class DroidTouchListener : Object, View.IOnTouchListener
    {
        public bool OnTouch(View v, MotionEvent e)
        {
            v.Parent?.RequestDisallowInterceptTouchEvent(true);
            if ((e.Action & MotionEventActions.Up) != 0 && (e.ActionMasked & MotionEventActions.Up) != 0)
                v.Parent?.RequestDisallowInterceptTouchEvent(false);
            return false;
        }
    }
}