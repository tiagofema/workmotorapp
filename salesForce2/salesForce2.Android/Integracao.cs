﻿using Android.App;
using Android.Content;
using Android.OS;
using Xamarin.Forms.Platform.Android;

namespace WorkmotorApp.Droid
{
    [Activity(Label = "Integracao")]
    [IntentFilter(new[] {Intent.ActionView},
        Categories = new[] {Intent.CategoryBrowsable, Intent.CategoryDefault},
        DataScheme = "http",
        DataHost = "workmotor.com.br",
        DataPathPrefix = "/fracasso",
        AutoVerify = false)]
    public class Integracao : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            LoadApplication(new App());
        }
    }
}