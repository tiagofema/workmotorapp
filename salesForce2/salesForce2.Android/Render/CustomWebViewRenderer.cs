﻿using WorkmotorApp.Droid.Render;
using WorkmotorApp.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebViewRenderer))]

namespace WorkmotorApp.Droid.Render
{
    public class CustomWebViewRenderer : WebViewRenderer
    {
        protected override void OnDetachedFromWindow()
        {
            base.OnDetachedFromWindow();
            // Make it stop w/e video or sound it's playing
            // http://forums.xamarin.com/discussion/43500/sound-in-webview-is-still-playing-in-the-background#latest
            // http://stackoverflow.com/questions/5946698/how-to-stop-youtube-video-playing-in-android-webview
            if (Control == null) return;
            Control.StopLoading();
            Control.LoadUrl("");
            Control.Reload();
        }
    }
}