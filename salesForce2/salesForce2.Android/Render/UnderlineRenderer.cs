﻿using Android.Graphics;
using WorkmotorApp.Droid.Render;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(UnderlineRenderer), typeof(UnderlineRenderer))]

namespace WorkmotorApp.Droid.Render
{
    public class UnderlineRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            if (Control != null) Control.PaintFlags = PaintFlags.UnderlineText;
        }
    }
}