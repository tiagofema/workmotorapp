﻿using Android.Views;
using WorkmotorApp.Droid.Render;
using WorkmotorApp.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;
using PickerRenderer = Xamarin.Forms.Platform.Android.AppCompat.PickerRenderer;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]

namespace WorkmotorApp.Droid.Render
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;
            //Centraliza o texto

            Control.Gravity = GravityFlags.CenterHorizontal;
            //Remove as linhas inferiores
            Control.SetBackgroundColor(Color.Transparent);
            Control.SetTextColor(Color.Black);

            //GradientDrawable gd = new GradientDrawable();
            //gd.SetStroke(0, Android.Graphics.Color.Transparent);
            //Control.SetBackground(gd);
        }
    }
}