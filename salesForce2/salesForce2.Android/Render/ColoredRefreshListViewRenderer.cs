﻿using System;
using Android.Support.V4.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;

namespace WorkmotorApp.Droid.Render
{
    [Preserve(AllMembers = true)]
    public class ColoredRefreshListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            try
            {
                var x = Control.Parent as SwipeRefreshLayout;
                x?.SetColorSchemeColors(Color.ParseColor("#223c74"));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
    }
}