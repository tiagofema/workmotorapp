﻿using System.ComponentModel;
using Android.Graphics.Drawables;
using Android.Runtime;
using WorkmotorApp.Droid.Render;
using WorkmotorApp.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer(typeof(NoUnderlineEditor), typeof(NoUnderlineEditorRenderer))]

namespace WorkmotorApp.Droid.Render
{
    public class NoUnderlineEditorRenderer : EditorRenderer
    {
        [Preserve(AllMembers = true)]
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var gd = new GradientDrawable();
                gd.SetColor(Color.Transparent);
                Control.SetBackgroundDrawable(gd);
                //Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.White));
            }

            if (e.NewElement != null)
            {
                var element = e.NewElement as NoUnderlineEditor;
                Control.Hint = element.Placeholder;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == NoUnderlineEditor.PlaceholderProperty.PropertyName)
            {
                var element = Element as NoUnderlineEditor;
                Control.Hint = element.Placeholder;
            }
        }
    }
}