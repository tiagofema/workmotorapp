﻿using Android.Content;
using Android.Views.InputMethods;
using WorkmotorApp.Droid.Render;
using WorkmotorApp.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NoKeyboardEntry), typeof(NoKeyboardEntryAndroidRenderer))]

namespace WorkmotorApp.Droid.Render
{
    public class NoKeyboardEntryAndroidRenderer : EntryRenderer
    {
        [Preserve(AllMembers = true)]
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null) ((NoKeyboardEntry) e.NewElement).PropertyChanging += OnPropertyChanging;

            if (e.OldElement != null) ((NoKeyboardEntry) e.OldElement).PropertyChanging -= OnPropertyChanging;

            // Disable the Keyboard on Focus
            Control.ShowSoftInputOnFocus = false;
        }

        private void OnPropertyChanging(object sender, PropertyChangingEventArgs propertyChangingEventArgs)
        {
            // Check if the view is about to get Focus
            if (propertyChangingEventArgs.PropertyName == VisualElement.IsFocusedProperty.PropertyName)
            {
                // incase if the focus was moved from another Entry
                // Forcefully dismiss the Keyboard 
                var imm = (InputMethodManager) Context.GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(Control.WindowToken, 0);
            }
        }
    }
}